package com.aswdc.propertytaxcalculator.activity

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.aswdc.propertytaxcalculator.R
import com.aswdc.propertytaxcalculator.activity.Design_Splash

class Design_Splash : AppCompatActivity() {
    protected var context: Context? = null
    var textView: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_design__splash)
        supportActionBar!!.hide()
        context = this@Design_Splash
        textView = findViewById(R.id.appname)
        val typeface = Typeface.createFromAsset(assets, "Montserrat-Bold.otf")
        textView.setTypeface(typeface)
        Handler().postDelayed({
            val `in` = Intent(this@Design_Splash, CityActivity::class.java)
            startActivity(`in`)
            finish()
        }, SPLASH_TIME_OUT.toLong())
    }

    companion object {
        private const val SPLASH_TIME_OUT = 3000
    }
}