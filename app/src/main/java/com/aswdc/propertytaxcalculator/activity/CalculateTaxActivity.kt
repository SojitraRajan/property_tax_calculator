package com.aswdc.propertytaxcalculator.activity

import android.Manifest.permission
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.aswdc.propertytaxcalculator.BottomSheet.BottomSheetAgeFactor
import com.aswdc.propertytaxcalculator.BottomSheet.BottomSheetBuildingFactor
import com.aswdc.propertytaxcalculator.BottomSheet.BottomSheetLocationFactor
import com.aswdc.propertytaxcalculator.BottomSheet.BottomSheetOccupancyFactor
import com.aswdc.propertytaxcalculator.DB_Helper.*
import com.aswdc.propertytaxcalculator.R
import com.aswdc.propertytaxcalculator.Utility.Constants
import com.aswdc.propertytaxcalculator.Utility.HeaderFooterPageEvent
import com.aswdc.propertytaxcalculator.adapter.Adapter_Agefactor
import com.aswdc.propertytaxcalculator.adapter.Adapter_Buildingfactor
import com.aswdc.propertytaxcalculator.adapter.Adapter_MarketLocation
import com.aswdc.propertytaxcalculator.adapter.Adapter_Occupancyfactor
import com.aswdc.propertytaxcalculator.model.*
import com.itextpdf.text.*
import com.itextpdf.text.html.HtmlTags
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class CalculateTaxActivity : AppCompatActivity() {
    @BindView(R.id.btn_residential)
    var btnResidential: TextView? = null

    @BindView(R.id.btn_nonresidential)
    var btnNonresidential: TextView? = null

    @BindView(R.id.sq_mt)
    var btnsqmt: TextView? = null

    @BindView(R.id.sq_ft)
    var btnsqft: TextView? = null

    @BindView(R.id.txtinfolocation)
    var imginfolocationfactor: ImageView? = null

    @BindView(R.id.txtinfoage)
    var imginfoage: ImageView? = null

    @BindView(R.id.txtinfobuilding)
    var imginfobuilding: ImageView? = null

    @BindView(R.id.txtinfooccupancy)
    var imginfooccupancy: ImageView? = null

    @BindView(R.id.reg_value)
    var regValue: EditText? = null

    @BindView(R.id.spinner_market_location)
    var spinnerMarketLocation: Spinner? = null

    @BindView(R.id.spinner_age_factor)
    var spinnerAgeFactor: Spinner? = null

    @BindView(R.id.spinner_building_factor)
    var spinnerBuildingFactor: Spinner? = null

    @BindView(R.id.spinner_occupancyfactor)
    var spinnerOccupancyfactor: Spinner? = null

    @BindView(R.id.tax_btn_calculate)
    var taxBtnCalculate: AppCompatButton? = null

    @BindView(R.id.shareitnow)
    var taxbtnshare: AppCompatButton? = null

    @BindView(R.id.propertyDetail)
    var calTxtPropertydetails: TextView? = null

    @BindView(R.id.carpet_area)
    var txtcarpetarea: TextView? = null

    @BindView(R.id.location_factor)
    var txtlocationfactor: TextView? = null

    @BindView(R.id.age_factor)
    var txtagefactor: TextView? = null

    @BindView(R.id.building_factor)
    var txtbuilingfactor: TextView? = null

    @BindView(R.id.occupancy_factor)
    var txtoccupancyfactor: TextView? = null

    @BindView(R.id.totaltax)
    var txttotaltax: TextView? = null

    @BindView(R.id.educationcharge)
    var txteducation: TextView? = null

    @BindView(R.id.garbagecharge)
    var txtgarbagecharge: TextView? = null

    @BindView(R.id.watercharge)
    var txtwatercharge: TextView? = null

    @BindView(R.id.fire)
    var txtfirecharge: TextView? = null

    @BindView(R.id.envinronment)
    var txtenvironment: TextView? = null

    @BindView(R.id.watgraindge)
    var txtwaterdraindge: TextView? = null

    @BindView(R.id.streetlight)
    var txtstreetlight: TextView? = null

    @BindView(R.id.conservancycharge)
    var txtconservancycharge: TextView? = null

    @BindView(R.id.totalpropertytax)
    var txttotalpropertytax: TextView? = null

    @BindView(R.id.cal_txt_carpet_area)
    var calTxtCarpetArea: TextView? = null

    @BindView(R.id.cal_txt_location_factor)
    var calTxtLocationFactor: TextView? = null

    @BindView(R.id.cal_txt_Age_factor)
    var calTxtAgeFactor: TextView? = null

    @BindView(R.id.cal_txt_building_factor)
    var calTxtBuildingFactor: TextView? = null

    @BindView(R.id.cal_txt_occupancy_factor)
    var calTxtOccupancyFactor: TextView? = null

    @BindView(R.id.cal_txt_normal_charge)
    var calTxtNormalCharge: TextView? = null

    @BindView(R.id.llcal_tax_gone)
    var llcalTaxGone: LinearLayout? = null

    @BindView(R.id.cal_txt_education_charge)
    var calTxtEducationCharge: TextView? = null

    @BindView(R.id.ll_education)
    var llEducation: LinearLayout? = null

    @BindView(R.id.cal_txt_garbage_charge)
    var calTxtGarbageCharge: TextView? = null

    @BindView(R.id.ll_garbage)
    var llGarbage: LinearLayout? = null

    @BindView(R.id.cal_txt_watercharge)
    var calTxtWatercharge: TextView? = null

    @BindView(R.id.ll_water)
    var llWater: LinearLayout? = null

    @BindView(R.id.cal_txt_conservancycharge)
    var calTxtConservancycharge: TextView? = null

    @BindView(R.id.ll_Conservancy)
    var llConservancy: LinearLayout? = null

    @BindView(R.id.cal_txt_water_drainage)
    var calTxtWaterDrainage: TextView? = null

    @BindView(R.id.ll_water_drainage)
    var llWaterDrainage: LinearLayout? = null

    @BindView(R.id.cal_txt_streeightlight)
    var calTxtStreeightlight: TextView? = null

    @BindView(R.id.cal_txt_fire)
    var calTxtfirecharge: TextView? = null

    @BindView(R.id.cal_txt_environment)
    var calTxtenvironment: TextView? = null

    @BindView(R.id.ll_street)
    var llStreet: LinearLayout? = null

    @BindView(R.id.ll_environment)
    var llEnvironment: LinearLayout? = null

    @BindView(R.id.ll_fire)
    var ll_fire: LinearLayout? = null

    @BindView(R.id.cal_txt_total_property_tax)
    var calTxtTotalPropertyTax: TextView? = null

    @BindView(R.id.ll_total_property)
    var llTotalProperty: LinearLayout? = null

    @BindView(R.id.linearlayout)
    var layout: LinearLayout? = null

    @BindView(R.id.llmainlayout)
    var llMainLayout: LinearLayout? = null

    @BindView(R.id.svmain)
    var svmainview: ScrollView? = null
    var context: Context? = null
    var tbl_Occupancyfactor: TblOccupancyfactor? = null
    var tbl_agefactor: TblAgefactor? = null
    var tbl_buildingfactor: TBLBuildingFactor? = null
    var tblMarketlocation: TblMarketlocation? = null
    var tblHistory: TblHistory? = null
    var modelhistory: Modelhistory? = null
    var Locationlist: ArrayList<ModelmarketLocation>? = null
    var Agefactorlist: ArrayList<Modelagefactor>? = null
    var Buildingfactorlist: ArrayList<Modelbuilingfactor>? = null
    var occupancylist: ArrayList<Modeloccupancyfactor>? = null
    var historyList: ArrayList<Modelhistory>? = null
    var cityID = 0
    var flag = 0
    var isResidential = true
    var locationWeitage = -1.0
    var agefactorWeitage = -1.0
    var buildingfactorWeitage = -1.0
    var occupancyWeitage = -1.0
    var ans = 0.0
    var carpetarea = 0.0
    var finaltax = 0.0
    var streetlight = 0.0
    var educationcess = 0.0
    var waterdrainage = 0.0
    var garbagecharge = 0.0
    var water = 0.0
    var conservancy = 0.0
    var enevironment = 0.0
    var firecharge = 0.0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculate_tax)
        ButterKnife.bind(this)
        dataFromIntent
        if (cityID == 1) {
            title = "Tax (Rajkot)"
        } else if (cityID == 2) {
            title = "Tax (Ahemdabad)"
        } else if (cityID == 3) {
            title = "Tax (Surat)"
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initReference()
        dataAndSetInSpinner
        setAdapter()
        setSelectionType()
        calculateTax()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.calculate_menu, menu)
        return true
    }

    fun setSelectionType() {
        btnResidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.left_unselected))
        btnResidential!!.setTextColor(ContextCompat.getColor(this, R.color.gray))
        btnNonresidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.right_unselected))
        btnNonresidential!!.setTextColor(ContextCompat.getColor(this, R.color.gray))
        btnsqft!!.setTextColor(ContextCompat.getColor(this, R.color.gray))
        btnsqmt!!.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary3))
        if (isResidential) {
            btnResidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.left_selected))
            btnResidential!!.setTextColor(ContextCompat.getColor(this, R.color.white))
            llcalTaxGone!!.visibility = View.GONE
            if (flag == 1) {
                btnsqmt!!.setTextColor(ContextCompat.getColor(context!!, R.color.gray))
                btnsqft!!.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary3))
            } else if (flag == 0) {
                btnsqft!!.setTextColor(ContextCompat.getColor(context!!, R.color.gray))
                btnsqmt!!.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary3))
            }
        } else {
            btnNonresidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.right_selected))
            btnNonresidential!!.setTextColor(ContextCompat.getColor(this, R.color.white))
            llcalTaxGone!!.visibility = View.GONE
            if (flag == 1) {
                btnsqmt!!.setTextColor(ContextCompat.getColor(context!!, R.color.gray))
                btnsqft!!.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary3))
            } else if (flag == 0) {
                btnsqft!!.setTextColor(ContextCompat.getColor(context!!, R.color.gray))
                btnsqmt!!.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary3))
            }
        }
        btnsqmt!!.setOnClickListener {
            btnsqft!!.setTextColor(ContextCompat.getColor(context!!, R.color.gray))
            btnsqmt!!.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary3))
            flag = 0
        }
        btnsqft!!.setOnClickListener {
            btnsqmt!!.setTextColor(ContextCompat.getColor(context!!, R.color.gray))
            btnsqft!!.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary3))
            flag = 1
        }
    }

    val dataFromIntent: Unit
        get() {
            cityID = intent.getIntExtra(Constants.CITY_ID, -1)
        }
    val dataAndSetInSpinner: Unit
        get() {
            Locationlist = tblMarketlocation!!.selectlocation(cityID)
            Agefactorlist = tbl_agefactor!!.selectlAgefactor(cityID)
            Buildingfactorlist = tbl_buildingfactor!!.selectBuildingfactor(cityID, propertyID)
            occupancylist = tbl_Occupancyfactor!!.selectOccupancyfactor(cityID, propertyID)
            historyList = tblHistory!!.getAllList(cityID, propertyID)
        }
    val propertyID: Int
        get() = if (isResidential) 1 else 2

    fun initReference() {
        tblMarketlocation = TblMarketlocation(applicationContext)
        tbl_agefactor = TblAgefactor(applicationContext)
        tbl_buildingfactor = TBLBuildingFactor(applicationContext)
        tbl_Occupancyfactor = TblOccupancyfactor(applicationContext)
        tblHistory = TblHistory(applicationContext)
        modelhistory = Modelhistory()
        context = this@CalculateTaxActivity
    }

    fun calculateTax() {
        taxBtnCalculate!!.setOnClickListener { view: View? ->
            if (isValid) if ((locationWeitage >= 0) && (agefactorWeitage >= 0) && (buildingfactorWeitage >= 0) && (occupancyWeitage >= 0)) {
                if (propertyID == 1 && cityID == 1) {
                    carpetarea = regValue!!.text.toString().toDouble()
                    if (flag == 1) carpetarea = carpetarea / 3.9
                    ans = (locationWeitage * agefactorWeitage * buildingfactorWeitage * occupancyWeitage * 11 * carpetarea)
                    water = 840.0
                    if (ans > 0 && ans <= 200) {
                        educationcess = ans + 0
                        finaltax = (ans + 365 + water)
                    } else if (ans >= 201 && ans <= 500) {
                        educationcess = (ans * 5) / 100
                        finaltax = (ans + 365 + educationcess + water)
                    } else if (ans >= 501 && ans <= 3000) {
                        educationcess = (ans * 10) / 100
                        finaltax = (ans + 365 + educationcess + water)
                    } else if (ans >= 3000) {
                        educationcess = (ans * 15) / 100
                        finaltax = (ans + 365 + educationcess + water)
                    }
                    garbagecharge = 365.0
                    if (flag == 1) {
                        carpetarea = carpetarea * 3.9
                        calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(ft)"
                    } else calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(m)²"
                    calTxtLocationFactor!!.text = "" + String.format("%.2f", locationWeitage)
                    calTxtAgeFactor!!.text = "" + String.format("%.2f", agefactorWeitage)
                    calTxtBuildingFactor!!.text = "" + String.format("%.2f", buildingfactorWeitage)
                    calTxtOccupancyFactor!!.text = "" + String.format("%.2f", occupancyWeitage)
                    calTxtNormalCharge!!.text = "" + "₹" + " " + String.format("%.2f", ans)
                    calTxtGarbageCharge!!.text = "" + "₹" + " " + String.format("%.2f", garbagecharge)
                    calTxtEducationCharge!!.text = "" + "₹" + " " + String.format("%.2f", educationcess)
                    calTxtWatercharge!!.text = "" + "₹" + " " + String.format("%.2f", water)
                    calTxtTotalPropertyTax!!.text = "" + "₹" + " " + String.format("%.2f", finaltax)
                    llGarbage!!.visibility = View.VISIBLE
                    llEducation!!.visibility = View.VISIBLE
                    llWater!!.visibility = View.VISIBLE
                    llTotalProperty!!.visibility = View.VISIBLE
                    if (calTxtCarpetArea!!.text.length > 0) {
                        modelhistory!!.carpetarea = calTxtCarpetArea!!.text.toString()
                    }
                    if (calTxtLocationFactor!!.text.length > 0) {
                        modelhistory!!.locationfactor = calTxtLocationFactor!!.text.toString()
                    }
                    if (calTxtAgeFactor!!.text.length > 0) {
                        modelhistory!!.agefactor = calTxtAgeFactor!!.text.toString()
                    }
                    if (calTxtBuildingFactor!!.text.length > 0) {
                        modelhistory!!.typeofbuiling = calTxtBuildingFactor!!.text.toString()
                    }
                    if (calTxtOccupancyFactor!!.text.length > 0) {
                        modelhistory!!.occupancyfactor = calTxtOccupancyFactor!!.text.toString()
                    }
                    if (calTxtNormalCharge!!.text.length > 0) {
                        modelhistory!!.totaltax = calTxtNormalCharge!!.text.toString()
                    }
                    if (calTxtGarbageCharge!!.text.length > 0) {
                        modelhistory!!.garbagecharge = calTxtGarbageCharge!!.text.toString()
                    }
                    if (calTxtEducationCharge!!.text.length > 0) {
                        modelhistory!!.educationcharge = calTxtEducationCharge!!.text.toString()
                    }
                    if (calTxtWatercharge!!.text.length > 0) {
                        modelhistory!!.watercharge = calTxtWatercharge!!.text.toString()
                    }
                    if (calTxtTotalPropertyTax!!.text.length > 0) {
                        modelhistory!!.totalpropertytax = calTxtTotalPropertyTax!!.text.toString()
                    }
                    modelhistory!!.cityId = cityID
                    modelhistory!!.propertyTypeId = propertyID
                    tblHistory!!.insert(modelhistory!!)
                } else if (propertyID == 2 && cityID == 1) {
                    carpetarea = regValue!!.text.toString().toDouble()
                    if (flag == 1) carpetarea = carpetarea / 3.9
                    ans = (locationWeitage * agefactorWeitage * buildingfactorWeitage * occupancyWeitage * 22 * carpetarea)
                    if (ans >= 0 && ans <= 200) {
                        educationcess = ans + 0
                        finaltax = (ans + 730)
                    } else if (ans >= 201 && ans <= 500) {
                        educationcess = (ans * 10) / 100
                        finaltax = (ans + 730 + educationcess)
                    } else if (ans >= 501 && ans <= 3000) {
                        educationcess = (ans * 20) / 100
                        finaltax = (ans + 730 + educationcess)
                    } else if (ans >= 3000) {
                        educationcess = (ans * 30) / 100
                        finaltax = (ans + 730 + educationcess)
                    }
                    garbagecharge = 730.0
                    if (flag == 1) {
                        carpetarea = carpetarea * 3.9
                        calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(ft)"
                    } else calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(m)²"
                    calTxtLocationFactor!!.text = "" + String.format("%.2f", locationWeitage)
                    calTxtAgeFactor!!.text = "" + String.format("%.2f", agefactorWeitage)
                    calTxtBuildingFactor!!.text = "" + String.format("%.2f", buildingfactorWeitage)
                    calTxtOccupancyFactor!!.text = "" + String.format("%.2f", occupancyWeitage)
                    calTxtEducationCharge!!.text = "" + "₹" + " " + String.format("%.2f", educationcess)
                    calTxtGarbageCharge!!.text = "" + "₹" + " " + String.format("%.2f", garbagecharge)
                    calTxtNormalCharge!!.text = "" + "₹" + " " + String.format("%.2f", ans)
                    calTxtTotalPropertyTax!!.text = "" + "₹" + " " + String.format("%.2f", finaltax)
                    llGarbage!!.visibility = View.VISIBLE
                    llWater!!.visibility = View.GONE
                    llEducation!!.visibility = View.VISIBLE
                    llTotalProperty!!.visibility = View.VISIBLE
                    if (calTxtCarpetArea!!.text.length > 0) {
                        modelhistory!!.carpetarea = calTxtCarpetArea!!.text.toString()
                    }
                    if (calTxtLocationFactor!!.text.length > 0) {
                        modelhistory!!.locationfactor = calTxtLocationFactor!!.text.toString()
                    }
                    if (calTxtAgeFactor!!.text.length > 0) {
                        modelhistory!!.agefactor = calTxtAgeFactor!!.text.toString()
                    }
                    if (calTxtBuildingFactor!!.text.length > 0) {
                        modelhistory!!.typeofbuiling = calTxtBuildingFactor!!.text.toString()
                    }
                    if (calTxtOccupancyFactor!!.text.length > 0) {
                        modelhistory!!.occupancyfactor = calTxtOccupancyFactor!!.text.toString()
                    }
                    if (calTxtNormalCharge!!.text.length > 0) {
                        modelhistory!!.totaltax = calTxtNormalCharge!!.text.toString()
                    }
                    if (calTxtGarbageCharge!!.text.length > 0) {
                        modelhistory!!.garbagecharge = calTxtGarbageCharge!!.text.toString()
                    }
                    if (calTxtEducationCharge!!.text.length > 0) {
                        modelhistory!!.educationcharge = calTxtEducationCharge!!.text.toString()
                    }
                    if (calTxtTotalPropertyTax!!.text.length > 0) {
                        modelhistory!!.totalpropertytax = calTxtTotalPropertyTax!!.text.toString()
                    }
                    modelhistory!!.cityId = cityID
                    modelhistory!!.propertyTypeId = propertyID
                    tblHistory!!.insert(modelhistory!!)
                } else if (propertyID == 1 && cityID == 2) {
                    carpetarea = regValue!!.text.toString().toDouble()
                    if (flag == 1) carpetarea = carpetarea / 3.9
                    ans = (locationWeitage * agefactorWeitage * buildingfactorWeitage * occupancyWeitage * carpetarea * 16)
                    if (ans > 0 && ans < 200) {
                        educationcess = ans + 0
                    } else if (ans > 201 && ans < 500) {
                        educationcess = (ans * 5) / 100
                    } else if (ans > 501 && ans < 3000) {
                        educationcess = (ans * 10) / 100
                    } else if (ans > 3000) {
                        educationcess = (ans * 15) / 100
                    }
                    water = (ans * 30) / 100
                    conservancy = (ans * 30) / 100
                    finaltax = (ans + water + conservancy + educationcess + 365)
                    garbagecharge = 365.0
                    if (flag == 1) {
                        carpetarea = carpetarea * 3.9
                        calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(ft)"
                    } else calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(m)²"
                    calTxtLocationFactor!!.text = "" + String.format("%.2f", locationWeitage)
                    calTxtAgeFactor!!.text = "" + String.format("%.2f", agefactorWeitage)
                    calTxtBuildingFactor!!.text = "" + String.format("%.2f", buildingfactorWeitage)
                    calTxtOccupancyFactor!!.text = "" + String.format("%.2f", occupancyWeitage)
                    calTxtNormalCharge!!.text = "" + "₹" + " " + String.format("%.2f", ans)
                    calTxtEducationCharge!!.text = "" + "₹" + " " + String.format("%.2f", educationcess)
                    calTxtGarbageCharge!!.text = "" + "₹" + " " + String.format("%.2f", garbagecharge)
                    calTxtWatercharge!!.text = "" + "₹" + " " + String.format("%.2f", water)
                    calTxtConservancycharge!!.text = "" + "₹" + " " + String.format("%.2f", conservancy)
                    calTxtTotalPropertyTax!!.text = "" + "₹" + " " + String.format("%.2f", finaltax)
                    llEducation!!.visibility = View.VISIBLE
                    llGarbage!!.visibility = View.VISIBLE
                    llWater!!.visibility = View.VISIBLE
                    llConservancy!!.visibility = View.VISIBLE
                    llTotalProperty!!.visibility = View.VISIBLE
                    if (calTxtCarpetArea!!.text.length > 0) {
                        modelhistory!!.carpetarea = calTxtCarpetArea!!.text.toString()
                    }
                    if (calTxtLocationFactor!!.text.length > 0) {
                        modelhistory!!.locationfactor = calTxtLocationFactor!!.text.toString()
                    }
                    if (calTxtAgeFactor!!.text.length > 0) {
                        modelhistory!!.agefactor = calTxtAgeFactor!!.text.toString()
                    }
                    if (calTxtBuildingFactor!!.text.length > 0) {
                        modelhistory!!.typeofbuiling = calTxtBuildingFactor!!.text.toString()
                    }
                    if (calTxtOccupancyFactor!!.text.length > 0) {
                        modelhistory!!.occupancyfactor = calTxtOccupancyFactor!!.text.toString()
                    }
                    if (calTxtNormalCharge!!.text.length > 0) {
                        modelhistory!!.totaltax = calTxtNormalCharge!!.text.toString()
                    }
                    if (calTxtGarbageCharge!!.text.length > 0) {
                        modelhistory!!.garbagecharge = calTxtGarbageCharge!!.text.toString()
                    }
                    if (calTxtEducationCharge!!.text.length > 0) {
                        modelhistory!!.educationcharge = calTxtEducationCharge!!.text.toString()
                    }
                    if (calTxtWatercharge!!.text.length > 0) {
                        modelhistory!!.watercharge = calTxtWatercharge!!.text.toString()
                    }
                    if (calTxtConservancycharge!!.text.length > 0) {
                        modelhistory!!.conservancycharge = calTxtConservancycharge!!.text.toString()
                    }
                    if (calTxtTotalPropertyTax!!.text.length > 0) {
                        modelhistory!!.totalpropertytax = calTxtTotalPropertyTax!!.text.toString()
                    }
                    modelhistory!!.cityId = cityID
                    modelhistory!!.propertyTypeId = propertyID
                    tblHistory!!.insert(modelhistory!!)
                } else if (propertyID == 2 && cityID == 2) {
                    carpetarea = regValue!!.text.toString().toDouble()
                    if (flag == 1) carpetarea = carpetarea / 3.9
                    ans = (locationWeitage * agefactorWeitage * buildingfactorWeitage * occupancyWeitage * carpetarea * 28)
                    if (ans > 0 && ans < 200) {
                        educationcess = ans + 0
                    } else if (ans > 201 && ans < 500) {
                        educationcess = (ans * 10) / 100
                    } else if (ans > 501 && ans < 3000) {
                        educationcess = (ans * 20) / 100
                    } else if (ans > 3000) {
                        educationcess = (ans * 30) / 100
                    }
                    water = (ans * 40) / 100
                    conservancy = (ans * 40) / 100
                    finaltax = (ans + water + conservancy + educationcess + 365)
                    garbagecharge = 365.0
                    if (flag == 1) {
                        carpetarea = carpetarea * 3.9
                        calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(ft)"
                    } else calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(m)²"
                    calTxtLocationFactor!!.text = "" + String.format("%.2f", locationWeitage)
                    calTxtAgeFactor!!.text = "" + String.format("%.2f", agefactorWeitage)
                    calTxtBuildingFactor!!.text = "" + String.format("%.2f", buildingfactorWeitage)
                    calTxtOccupancyFactor!!.text = "" + String.format("%.2f", occupancyWeitage)
                    calTxtNormalCharge!!.text = "" + "₹" + " " + String.format("%.2f", ans)
                    calTxtEducationCharge!!.text = "" + "₹" + " " + String.format("%.2f", educationcess)
                    calTxtGarbageCharge!!.text = "" + "₹" + " " + String.format("%.2f", garbagecharge)
                    calTxtWatercharge!!.text = "" + "₹" + " " + String.format("%.2f", water)
                    calTxtConservancycharge!!.text = "" + "₹" + " " + String.format("%.2f", conservancy)
                    calTxtTotalPropertyTax!!.text = "" + "₹" + " " + String.format("%.2f", finaltax)
                    llEducation!!.visibility = View.VISIBLE
                    llGarbage!!.visibility = View.VISIBLE
                    llWater!!.visibility = View.VISIBLE
                    llConservancy!!.visibility = View.VISIBLE
                    llTotalProperty!!.visibility = View.VISIBLE
                    if (calTxtCarpetArea!!.text.length > 0) {
                        modelhistory!!.carpetarea = calTxtCarpetArea!!.text.toString()
                    }
                    if (calTxtLocationFactor!!.text.length > 0) {
                        modelhistory!!.locationfactor = calTxtLocationFactor!!.text.toString()
                    }
                    if (calTxtAgeFactor!!.text.length > 0) {
                        modelhistory!!.agefactor = calTxtAgeFactor!!.text.toString()
                    }
                    if (calTxtBuildingFactor!!.text.length > 0) {
                        modelhistory!!.typeofbuiling = calTxtBuildingFactor!!.text.toString()
                    }
                    if (calTxtOccupancyFactor!!.text.length > 0) {
                        modelhistory!!.occupancyfactor = calTxtOccupancyFactor!!.text.toString()
                    }
                    if (calTxtNormalCharge!!.text.length > 0) {
                        modelhistory!!.totaltax = calTxtNormalCharge!!.text.toString()
                    }
                    if (calTxtGarbageCharge!!.text.length > 0) {
                        modelhistory!!.garbagecharge = calTxtGarbageCharge!!.text.toString()
                    }
                    if (calTxtEducationCharge!!.text.length > 0) {
                        modelhistory!!.educationcharge = calTxtEducationCharge!!.text.toString()
                    }
                    if (calTxtWatercharge!!.text.length > 0) {
                        modelhistory!!.watercharge = calTxtWatercharge!!.text.toString()
                    }
                    if (calTxtConservancycharge!!.text.length > 0) {
                        modelhistory!!.conservancycharge = calTxtConservancycharge!!.text.toString()
                    }
                    if (calTxtTotalPropertyTax!!.text.length > 0) {
                        modelhistory!!.totalpropertytax = calTxtTotalPropertyTax!!.text.toString()
                    }
                    modelhistory!!.cityId = cityID
                    modelhistory!!.propertyTypeId = propertyID
                    tblHistory!!.insert(modelhistory!!)
                } else if (propertyID == 1 && cityID == 3) {
                    carpetarea = regValue!!.text.toString().toDouble()
                    if (flag == 1) carpetarea = carpetarea / 3.9
                    ans = (locationWeitage * agefactorWeitage * buildingfactorWeitage * occupancyWeitage * carpetarea * 10)
                    if (ans > 0 && ans < 200) {
                        educationcess = ans + 0
                    } else if (ans > 201 && ans < 500) {
                        educationcess = (ans * 5) / 100
                    } else if (ans > 501 && ans < 3000) {
                        educationcess = (ans * 10) / 100
                    } else if (ans > 3000) {
                        educationcess = (ans * 15) / 100
                    }
                    if (carpetarea > 0 && carpetarea <= 15) {
                        waterdrainage = 470.0
                    } else if (carpetarea >= 16 && carpetarea <= 25) {
                        waterdrainage = 810.0
                    } else if (carpetarea >= 26 && carpetarea <= 50) {
                        waterdrainage = 1296.0
                    } else if (carpetarea >= 51 && carpetarea <= 100) {
                        waterdrainage = 1944.0
                    } else if (carpetarea >= 101 && carpetarea <= 200) {
                        waterdrainage = 2835.0
                    } else if (carpetarea >= 201 && carpetarea <= 500) {
                        waterdrainage = 5064.0
                    } else if (carpetarea >= 501) {
                        waterdrainage = 10125.0
                    }
                    if (carpetarea >= 0 && carpetarea <= 50) {
                        garbagecharge = 810.0
                    }
                    if (carpetarea >= 51 && carpetarea <= 100) {
                        garbagecharge = 972.0
                    }
                    if (carpetarea >= 101 && carpetarea <= 200) {
                        garbagecharge = 1296.0
                    }
                    if (carpetarea >= 201 && carpetarea <= 400) {
                        garbagecharge = 1620.0
                    }
                    if (carpetarea >= 401 && carpetarea <= 500) {
                        garbagecharge = 2025.0
                    }
                    if (carpetarea > 500) {
                        garbagecharge = 2430.0
                    }
                    if (carpetarea >= 0 && carpetarea <= 25) {
                        enevironment = 0.0
                    }
                    if (carpetarea >= 26 && carpetarea <= 50) {
                        enevironment = 25.0
                    }
                    if (carpetarea >= 51 && carpetarea <= 100) {
                        enevironment = 30.0
                    }
                    if (carpetarea >= 101 && carpetarea <= 200) {
                        enevironment = 175.0
                    }
                    if (carpetarea >= 201 && carpetarea <= 400) {
                        enevironment = 300.0
                    }
                    if (carpetarea >= 401 && carpetarea <= 500) {
                        enevironment = 450.0
                    }
                    if (carpetarea > 500) {
                        enevironment = 625.0
                    }
                    if (carpetarea >= 0 && carpetarea <= 25) {
                        firecharge = 0.0
                    }
                    if (carpetarea >= 26 && carpetarea <= 50) {
                        firecharge = 30.0
                    }
                    if (carpetarea >= 51 && carpetarea <= 100) {
                        firecharge = 40.0
                    }
                    if (carpetarea >= 101 && carpetarea <= 200) {
                        firecharge = 75.0
                    }
                    if (carpetarea >= 201 && carpetarea <= 400) {
                        firecharge = 150.0
                    }
                    if (carpetarea >= 401 && carpetarea <= 500) {
                        firecharge = 200.0
                    }
                    if (carpetarea > 500) {
                        firecharge = 300.0
                    }
                    finaltax = (ans + 50 + educationcess + waterdrainage + garbagecharge + firecharge + enevironment)
                    streetlight = 50.0
                    if (flag == 1) {
                        carpetarea = carpetarea * 3.9
                        calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(ft)"
                    } else calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(m)²"
                    calTxtLocationFactor!!.text = "" + String.format("%.2f", locationWeitage)
                    calTxtAgeFactor!!.text = "" + String.format("%.2f", agefactorWeitage)
                    calTxtBuildingFactor!!.text = "" + String.format("%.2f", buildingfactorWeitage)
                    calTxtOccupancyFactor!!.text = "" + String.format("%.2f", occupancyWeitage)
                    calTxtNormalCharge!!.text = "" + "₹" + " " + String.format("%.2f", ans)
                    calTxtGarbageCharge!!.text = "" + "₹" + " " + String.format("%.2f", garbagecharge)
                    calTxtenvironment!!.text = "" + "₹" + " " + String.format("%.2f", enevironment)
                    calTxtfirecharge!!.text = "" + "₹" + " " + String.format("%.2f", firecharge)
                    calTxtWaterDrainage!!.text = "" + "₹" + " " + String.format("%.2f", waterdrainage)
                    calTxtEducationCharge!!.text = "" + "₹" + " " + String.format("%.2f", educationcess)
                    calTxtStreeightlight!!.text = "" + "₹" + " " + String.format("%.2f", streetlight)
                    calTxtTotalPropertyTax!!.text = "" + "₹" + " " + String.format("%.2f", finaltax)
                    ll_fire!!.visibility = View.VISIBLE
                    llEnvironment!!.visibility = View.VISIBLE
                    llEducation!!.visibility = View.VISIBLE
                    llWaterDrainage!!.visibility = View.VISIBLE
                    llGarbage!!.visibility = View.VISIBLE
                    llStreet!!.visibility = View.VISIBLE
                    llTotalProperty!!.visibility = View.VISIBLE
                    if (calTxtCarpetArea!!.text.length > 0) {
                        modelhistory!!.carpetarea = calTxtCarpetArea!!.text.toString()
                    }
                    if (calTxtLocationFactor!!.text.length > 0) {
                        modelhistory!!.locationfactor = calTxtLocationFactor!!.text.toString()
                    }
                    if (calTxtAgeFactor!!.text.length > 0) {
                        modelhistory!!.agefactor = calTxtAgeFactor!!.text.toString()
                    }
                    if (calTxtBuildingFactor!!.text.length > 0) {
                        modelhistory!!.typeofbuiling = calTxtBuildingFactor!!.text.toString()
                    }
                    if (calTxtOccupancyFactor!!.text.length > 0) {
                        modelhistory!!.occupancyfactor = calTxtOccupancyFactor!!.text.toString()
                    }
                    if (calTxtNormalCharge!!.text.length > 0) {
                        modelhistory!!.totaltax = calTxtNormalCharge!!.text.toString()
                    }
                    if (calTxtGarbageCharge!!.text.length > 0) {
                        modelhistory!!.garbagecharge = calTxtGarbageCharge!!.text.toString()
                    }
                    if (calTxtEducationCharge!!.text.length > 0) {
                        modelhistory!!.educationcharge = calTxtEducationCharge!!.text.toString()
                    }
                    if (calTxtWaterDrainage!!.text.length > 0) {
                        modelhistory!!.wateranddrainagecharge = calTxtWaterDrainage!!.text.toString()
                    }
                    if (calTxtenvironment!!.text.length > 0) {
                        modelhistory!!.environmentcharge = calTxtenvironment!!.text.toString()
                    }
                    if (calTxtfirecharge!!.text.length > 0) {
                        modelhistory!!.firecharge = calTxtfirecharge!!.text.toString()
                    }
                    if (calTxtStreeightlight!!.text.length > 0) {
                        modelhistory!!.streetlightcharge = calTxtStreeightlight!!.text.toString()
                    }
                    if (calTxtTotalPropertyTax!!.text.length > 0) {
                        modelhistory!!.totalpropertytax = calTxtTotalPropertyTax!!.text.toString()
                    }
                    modelhistory!!.cityId = cityID
                    modelhistory!!.propertyTypeId = propertyID
                    tblHistory!!.insert(modelhistory!!)
                } else if (propertyID == 2 && cityID == 3) {
                    carpetarea = regValue!!.text.toString().toDouble()
                    if (flag == 1) carpetarea = carpetarea / 3.9
                    ans = (locationWeitage * agefactorWeitage * buildingfactorWeitage * occupancyWeitage * carpetarea * 25)
                    if (ans > 0 && ans < 200) {
                        educationcess = ans + 0
                    } else if (ans > 201 && ans < 500) {
                        educationcess = (ans * 10) / 100
                    } else if (ans > 501 && ans < 3000) {
                        educationcess = (ans * 20) / 100
                    } else if (ans > 3000) {
                        educationcess = (ans * 30) / 100
                    }
                    if (carpetarea >= 0 && carpetarea <= 50) {
                        garbagecharge = 810.0
                    }
                    if (carpetarea >= 51 && carpetarea <= 100) {
                        garbagecharge = 1215.0
                    }
                    if (carpetarea >= 101 && carpetarea <= 200) {
                        garbagecharge = 1458.0
                    }
                    if (carpetarea >= 201 && carpetarea <= 400) {
                        garbagecharge = 1944.0
                    }
                    if (carpetarea >= 401 && carpetarea <= 500) {
                        garbagecharge = 2430.0
                    }
                    if (carpetarea > 500) {
                        garbagecharge = 3645.0
                    }
                    if (carpetarea > 0 && carpetarea <= 15) {
                        waterdrainage = 810.0
                    } else if (carpetarea >= 16 && carpetarea <= 25) {
                        waterdrainage = 1458.0
                    } else if (carpetarea >= 26 && carpetarea <= 50) {
                        waterdrainage = 1944.0
                    } else if (carpetarea >= 51 && carpetarea <= 100) {
                        waterdrainage = 3240.0
                    } else if (carpetarea >= 101 && carpetarea <= 200) {
                        waterdrainage = 4860.0
                    } else if (carpetarea >= 201 && carpetarea <= 500) {
                        waterdrainage = 8100.0
                    } else if (carpetarea >= 501) {
                        waterdrainage = 15188.0
                    }
                    if (carpetarea >= 0 && carpetarea <= 25) {
                        enevironment = 30.0
                    }
                    if (carpetarea >= 26 && carpetarea <= 50) {
                        enevironment = 40.0
                    }
                    if (carpetarea >= 51 && carpetarea <= 100) {
                        enevironment = 50.0
                    }
                    if (carpetarea >= 101 && carpetarea <= 200) {
                        enevironment = 350.0
                    }
                    if (carpetarea >= 201 && carpetarea <= 400) {
                        enevironment = 600.0
                    }
                    if (carpetarea >= 401 && carpetarea <= 500) {
                        enevironment = 900.0
                    }
                    if (carpetarea > 500) {
                        enevironment = 1250.0
                    }
                    if (carpetarea >= 0 && carpetarea <= 25) {
                        firecharge = 0.0
                    }
                    if (carpetarea >= 26 && carpetarea <= 50) {
                        firecharge = 30.0
                    }
                    if (carpetarea >= 51 && carpetarea <= 100) {
                        firecharge = 40.0
                    }
                    if (carpetarea >= 101 && carpetarea <= 200) {
                        firecharge = 75.0
                    }
                    if (carpetarea >= 201 && carpetarea <= 400) {
                        firecharge = 150.0
                    }
                    if (carpetarea >= 401 && carpetarea <= 500) {
                        firecharge = 200.0
                    }
                    if (carpetarea > 500) {
                        firecharge = 300.0
                    }
                    finaltax = (ans + 100 + educationcess + waterdrainage + garbagecharge + enevironment + 300)
                    streetlight = 24.0
                    if (flag == 1) {
                        carpetarea = carpetarea * 3.9
                        calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(ft)"
                    } else calTxtCarpetArea!!.text = "" + String.format("%.2f", carpetarea) + " " + "(m)²"
                    calTxtLocationFactor!!.text = "" + String.format("%.2f", locationWeitage)
                    calTxtAgeFactor!!.text = "" + String.format("%.2f", agefactorWeitage)
                    calTxtBuildingFactor!!.text = "" + String.format("%.2f", buildingfactorWeitage)
                    calTxtOccupancyFactor!!.text = "" + String.format("%.2f", occupancyWeitage)
                    calTxtNormalCharge!!.text = "" + "₹" + " " + String.format("%.2f", ans)
                    calTxtGarbageCharge!!.text = "" + "₹" + " " + String.format("%.2f", garbagecharge)
                    calTxtenvironment!!.text = "" + "₹" + " " + String.format("%.2f", enevironment)
                    calTxtfirecharge!!.text = "" + "₹" + " " + String.format("%.2f", firecharge)
                    calTxtWaterDrainage!!.text = "" + "₹" + " " + String.format("%.2f", waterdrainage)
                    calTxtEducationCharge!!.text = "" + "₹" + " " + String.format("%.2f", educationcess)
                    calTxtStreeightlight!!.text = "" + "₹" + " " + String.format("%.2f", streetlight)
                    calTxtTotalPropertyTax!!.text = "" + "₹" + " " + String.format("%.2f", finaltax)
                    ll_fire!!.visibility = View.VISIBLE
                    llEnvironment!!.visibility = View.VISIBLE
                    llEducation!!.visibility = View.VISIBLE
                    llWaterDrainage!!.visibility = View.VISIBLE
                    llGarbage!!.visibility = View.VISIBLE
                    llStreet!!.visibility = View.VISIBLE
                    llTotalProperty!!.visibility = View.VISIBLE
                    if (calTxtCarpetArea!!.text.length > 0) {
                        modelhistory!!.carpetarea = calTxtCarpetArea!!.text.toString()
                    }
                    if (calTxtLocationFactor!!.text.length > 0) {
                        modelhistory!!.locationfactor = calTxtLocationFactor!!.text.toString()
                    }
                    if (calTxtAgeFactor!!.text.length > 0) {
                        modelhistory!!.agefactor = calTxtAgeFactor!!.text.toString()
                    }
                    if (calTxtBuildingFactor!!.text.length > 0) {
                        modelhistory!!.typeofbuiling = calTxtBuildingFactor!!.text.toString()
                    }
                    if (calTxtOccupancyFactor!!.text.length > 0) {
                        modelhistory!!.occupancyfactor = calTxtOccupancyFactor!!.text.toString()
                    }
                    if (calTxtNormalCharge!!.text.length > 0) {
                        modelhistory!!.totaltax = calTxtNormalCharge!!.text.toString()
                    }
                    if (calTxtGarbageCharge!!.text.length > 0) {
                        modelhistory!!.garbagecharge = calTxtGarbageCharge!!.text.toString()
                    }
                    if (calTxtEducationCharge!!.text.length > 0) {
                        modelhistory!!.educationcharge = calTxtEducationCharge!!.text.toString()
                    }
                    if (calTxtWaterDrainage!!.text.length > 0) {
                        modelhistory!!.wateranddrainagecharge = calTxtWaterDrainage!!.text.toString()
                    }
                    if (calTxtStreeightlight!!.text.length > 0) {
                        modelhistory!!.streetlightcharge = calTxtStreeightlight!!.text.toString()
                    }
                    if (calTxtenvironment!!.text.length > 0) {
                        modelhistory!!.environmentcharge = calTxtenvironment!!.text.toString()
                    }
                    if (calTxtfirecharge!!.text.length > 0) {
                        modelhistory!!.firecharge = calTxtfirecharge!!.text.toString()
                    }
                    if (calTxtTotalPropertyTax!!.text.length > 0) {
                        modelhistory!!.totalpropertytax = calTxtTotalPropertyTax!!.text.toString()
                    }
                    modelhistory!!.cityId = cityID
                    modelhistory!!.propertyTypeId = propertyID
                    tblHistory!!.insert(modelhistory!!)
                }
                llcalTaxGone!!.visibility = View.VISIBLE
                svmainview!!.post(Runnable { svmainview!!.fullScroll(ScrollView.FOCUS_DOWN) })
            }
        }
        imginfolocationfactor!!.setOnClickListener {
            val bottomSheetLocationFactor = BottomSheetLocationFactor()
            bottomSheetLocationFactor.show(supportFragmentManager, "TAG")
        }
        imginfoage!!.setOnClickListener {
            val bottomSheet = BottomSheetAgeFactor()
            bottomSheet.show(supportFragmentManager, "TAG")
        }
        imginfobuilding!!.setOnClickListener {
            val bottomSheet = BottomSheetBuildingFactor()
            bottomSheet.show(supportFragmentManager, "TAG")
        }
        imginfooccupancy!!.setOnClickListener {
            val bottomSheet = BottomSheetOccupancyFactor()
            bottomSheet.show(supportFragmentManager, "TAG")
        }
        taxbtnshare!!.setOnClickListener {
            try {
                if (checkPermission()) createPDF() else {
                    requestPermission()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: DocumentException) {
                e.printStackTrace()
            }
        }
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this.applicationContext, permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(permission.WRITE_EXTERNAL_STORAGE), 200)
    }

    @Throws(IOException::class, DocumentException::class)
    fun createPDF() {
        val doc = Document(PageSize.A4, 36F, 36F, 90F, 36F)
        val directoryName = "Property Tax Calculator" + File.separator + "Property Tax calculator"
        val fileName = "PropertyTax.pdf"
        val path = Environment.getExternalStorageDirectory().absoluteFile.toString() + File.separator + directoryName
        val root = File(path)
        if (!root.exists()) root.mkdirs()
        val writer = PdfWriter.getInstance(doc,
                FileOutputStream(Environment.getExternalStorageDirectory().toString() + File.separator + directoryName + File.separator + fileName))
        Log.d("FilePath", this.application.getExternalFilesDir("Return").toString() + "/" + fileName)
        if (cityID == 1) {
            val strHeader = "Rajkot"
            val event = HeaderFooterPageEvent(this, strHeader)
            writer.pageEvent = event
        } else if (cityID == 2) {
            val strHeader = "Ahmedabad"
            val event = HeaderFooterPageEvent(this, strHeader)
            writer.pageEvent = event
        }
        if (cityID == 3) {
            val strHeader = "Surat"
            val event = HeaderFooterPageEvent(this, strHeader)
            writer.pageEvent = event
        }
        doc.open()
        doc.newPage()
        val reportBody = Paragraph()
        reportBody.alignment = Paragraph.ALIGN_LEFT
        val columnwidths = floatArrayOf(1.5f, 1f)
        val f1 = FontFactory.getFont(HtmlTags.FONT, 30f)
        f1.style = Font.BOLD
        val f2 = FontFactory.getFont(HtmlTags.FONT, 20f)
        f2.style = Font.BOLD
        val f8 = FontFactory.getFont(HtmlTags.FONT, 16f)
        f8.style = Font.BOLD
        val table = PdfPTable(columnwidths)
        table.widthPercentage = 100f
        table.defaultCell.isUseAscender = true
        val cell21 = PdfPCell(Phrase("Property Detail", f2))
        cell21.horizontalAlignment = Element.ALIGN_BASELINE or Element.ALIGN_BOTTOM
        cell21.border = Rectangle.BOTTOM
        cell21.paddingTop = 40f
        cell21.paddingBottom = 20f
        cell21.paddingRight = 10f
        val cell22 = PdfPCell(Phrase("  ", f2))
        cell22.horizontalAlignment = Element.ALIGN_BASELINE or Element.ALIGN_BOTTOM
        cell22.border = Rectangle.BOTTOM
        cell22.paddingTop = 40f
        cell22.paddingBottom = 20f
        cell22.paddingLeft = 10f
        cell22.paddingRight = 10f
        table.addCell(cell21)
        table.addCell(cell22)
        val mText = calTxtCarpetArea!!.text.toString()
        val mText1 = calTxtLocationFactor!!.text.toString()
        val mText2 = calTxtAgeFactor!!.text.toString()
        val mText3 = calTxtBuildingFactor!!.text.toString()
        val mText4 = calTxtOccupancyFactor!!.text.toString()
        val mText5 = calTxtNormalCharge!!.text.toString()
        val mText6 = calTxtTotalPropertyTax!!.text.toString()
        val rgarbage = calTxtGarbageCharge!!.text.toString()
        val reducation = calTxtEducationCharge!!.text.toString()
        val rwater = calTxtWatercharge!!.text.toString()
        val rngarbage = calTxtGarbageCharge!!.text.toString()
        val rneducation = calTxtEducationCharge!!.text.toString()
        val agarbage = calTxtGarbageCharge!!.text.toString()
        val aeducation = calTxtEducationCharge!!.text.toString()
        val awater = calTxtWatercharge!!.text.toString()
        val aconversancy = calTxtConservancycharge!!.text.toString()
        val aneducation = calTxtEducationCharge!!.text.toString()
        val anwater = calTxtWatercharge!!.text.toString()
        val anconversancy = calTxtConservancycharge!!.text.toString()
        val sgarbage = calTxtGarbageCharge!!.text.toString()
        val seducation = calTxtEducationCharge!!.text.toString()
        val senvrionment = calTxtenvironment!!.text.toString()
        val sfire = calTxtfirecharge!!.text.toString()
        val swaterdraindge = calTxtWaterDrainage!!.text.toString()
        val sstreetlight = calTxtStreeightlight!!.text.toString()
        val sngarbage = calTxtGarbageCharge!!.text.toString()
        val sneducation = calTxtEducationCharge!!.text.toString()
        val snenvrionment = calTxtenvironment!!.text.toString()
        val snfire = calTxtfirecharge!!.text.toString()
        val snwaterdraindge = calTxtWaterDrainage!!.text.toString()
        val snstreetlight = calTxtStreeightlight!!.text.toString()
        if (cityID == 1 && propertyID == 1) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Residential" + " "))
        }
        if (cityID == 1 && propertyID == 2) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Non-Residential" + " "))
        }
        if (cityID == 2 && propertyID == 1) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Residential" + " "))
        }
        if (cityID == 2 && propertyID == 2) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Non-Residential" + " "))
        }
        if (cityID == 3 && propertyID == 1) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Residential" + " "))
        }
        if (cityID == 3 && propertyID == 2) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Non-Residential" + " "))
        }
        table.addCell(setCellTitle("Carpet Area"))
        table.addCell(setCellTitle1(" $mText "))
        table.addCell(setCellTitle("Location Factor"))
        table.addCell(setCellTitle1("  $mText1 "))
        table.addCell(setCellTitle("Age Factor"))
        table.addCell(setCellTitle1("  $mText2 "))
        table.addCell(setCellTitle("Building  Factor"))
        table.addCell(setCellTitle1("  $mText3 "))
        table.addCell(setCellTitle("Occupancy  Factor"))
        table.addCell(setCellTitle1("  $mText4 "))
        table.addCell(setCellTitle("Total  Tax"))
        table.addCell(setCellvalue1("$mText5 "))
        if (cityID == 1 && propertyID == 1) {
            table.addCell(setCellTitle("Garbage Charge"))
            table.addCell(setCellvalue1("$rgarbage "))
            table.addCell(setCellTitle("Education Charge"))
            table.addCell(setCellvalue1("$reducation "))
            table.addCell(setCellTitle("Water Charge"))
            table.addCell(setCellvalue1("$rwater "))
        } else if (cityID == 1 && propertyID == 2) {
            table.addCell(setCellTitle("Garbage Charge"))
            table.addCell(setCellvalue1("$rngarbage "))
            table.addCell(setCellTitle("Education Charge"))
            table.addCell(setCellvalue1("$rneducation "))
        } else if (cityID == 2 && propertyID == 1) {
            table.addCell(setCellTitle("Garbage Charge"))
            table.addCell(setCellvalue1("$agarbage "))
            table.addCell(setCellTitle("Education Charge"))
            table.addCell(setCellvalue1("$aeducation "))
            table.addCell(setCellTitle("Water Charge"))
            table.addCell(setCellvalue1("$awater "))
            table.addCell(setCellTitle("Conservancy Charge"))
            table.addCell(setCellvalue1("$aconversancy "))
        } else if (cityID == 2 && propertyID == 2) {
            table.addCell(setCellTitle("Garbage Charge"))
            table.addCell(setCellvalue1("$agarbage "))
            table.addCell(setCellTitle("Education Charge"))
            table.addCell(setCellvalue1("$aneducation "))
            table.addCell(setCellTitle("Water Charge"))
            table.addCell(setCellvalue1("$anwater "))
            table.addCell(setCellTitle("Conservancy Charge"))
            table.addCell(setCellvalue1("$anconversancy "))
        } else if (cityID == 3 && propertyID == 1) {
            table.addCell(setCellTitle("Garbage Charge"))
            table.addCell(setCellvalue1("$sgarbage "))
            table.addCell(setCellTitle("Education Charge"))
            table.addCell(setCellvalue1("$seducation "))
            table.addCell(setCellTitle("Waterdraindge Charge"))
            table.addCell(setCellvalue1("$swaterdraindge "))
            table.addCell(setCellTitle("Fire Charge"))
            table.addCell(setCellvalue1("$sfire "))
            table.addCell(setCellTitle("Environment Charge"))
            table.addCell(setCellvalue1("$senvrionment "))
            table.addCell(setCellTitle("Streetlight Charge"))
            table.addCell(setCellvalue1("$sstreetlight "))
        } else if (cityID == 3 && propertyID == 2) {
            table.addCell(setCellTitle("Garbage Charge"))
            table.addCell(setCellvalue1("$sngarbage "))
            table.addCell(setCellTitle("Education Charge"))
            table.addCell(setCellvalue1("$sneducation "))
            table.addCell(setCellTitle("Waterdraindge Charge"))
            table.addCell(setCellvalue1("$snwaterdraindge "))
            table.addCell(setCellTitle("Fire Charge"))
            table.addCell(setCellvalue1("$snfire "))
            table.addCell(setCellTitle("Environment Charge"))
            table.addCell(setCellvalue1("$snenvrionment "))
            table.addCell(setCellTitle("Streetlight Charge"))
            table.addCell(setCellvalue1("$snstreetlight "))
        }
        table.addCell(setHeaderCellvalue("Total Property Tax", f8, 1))
        table.addCell(setHeaderCellvalue1("$mText6 ", f8, 2))
        reportBody.add(table)
        doc.add(reportBody)
        doc.close()
        viewPdf(fileName, directoryName)
    }

    fun setCellTitle(str: String?): PdfPCell {
        val f3 = FontFactory.getFont(HtmlTags.FONT, 16f)
        f3.setColor(0, 0, 0)
        val cell41 = PdfPCell(Phrase(str, f3))
        cell41.border = Rectangle.LEFT or Rectangle.BOTTOM or Rectangle.RIGHT
        cell41.horizontalAlignment = Element.ALIGN_BASELINE or Element.ALIGN_BOTTOM
        cell41.setPadding(8f)
        return cell41
    }

    fun setCellTitle1(str: String): PdfPCell {
        val f3 = FontFactory.getFont(HtmlTags.FONT, 16f)
        f3.setColor(0, 0, 0)
        val cell41 = PdfPCell(Phrase(str, f3))
        val p = Paragraph()
        cell41.border = Rectangle.LEFT or Rectangle.BOTTOM or Rectangle.RIGHT
        cell41.horizontalAlignment = Element.ALIGN_BASELINE or Element.ALIGN_BOTTOM
        cell41.setPadding(8f)
        p.add(Phrase(" $str", f3))
        p.alignment = Element.ALIGN_RIGHT
        cell41.addElement(p)
        return cell41
    }

    @Throws(IOException::class, DocumentException::class)
    fun setCellvalue1(str: String): PdfPCell {
        val f4 = FontFactory.getFont(HtmlTags.FONT, 16f)
        f4.setColor(0, 0, 0)
        val cell42 = PdfPCell()
        cell42.border = Rectangle.BOTTOM or Rectangle.RIGHT //new Phrase(str, f4));
        val p = Paragraph()
        cell42.horizontalAlignment = Element.ALIGN_BASELINE or Element.ALIGN_RIGHT
        p.add(Chunk(setImage(), 0F, 0F, true))
        p.add(Phrase(" $str", f4))
        p.alignment = Element.ALIGN_RIGHT
        cell42.addElement(p)
        cell42.setPadding(8f)
        return cell42
    }

    fun setHeaderCellvalue(str: String?, f: Font?, poition: Int): PdfPCell {
        val cellvalues = PdfPCell(Phrase(str, f))
        cellvalues.horizontalAlignment = Element.ALIGN_CENTER
        cellvalues.border = Rectangle.BOX
        cellvalues.setPadding(8f)
        cellvalues.backgroundColor = BaseColor.WHITE
        if (poition == 0) cellvalues.horizontalAlignment = Element.ALIGN_CENTER else if (poition == 1) cellvalues.horizontalAlignment = Element.ALIGN_LEFT else if (poition == 2) cellvalues.horizontalAlignment = Element.ALIGN_RIGHT
        return cellvalues
    }

    fun setHeaderCellvalue1(str: String, f: Font?, poition: Int): PdfPCell {
        val cellvalues = PdfPCell(Phrase(str, f))
        cellvalues.horizontalAlignment = Element.ALIGN_CENTER
        cellvalues.border = Rectangle.BOX
        cellvalues.setPadding(5f)
        val p = Paragraph()
        try {
            p.add(Chunk(setImage(), 0F, 0F, true))
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: BadElementException) {
            e.printStackTrace()
        }
        p.alignment = Element.ALIGN_RIGHT
        p.add(Phrase(" $str", f))
        cellvalues.addElement(p)
        cellvalues.backgroundColor = BaseColor.WHITE
        if (poition == 0) cellvalues.horizontalAlignment = Element.ALIGN_CENTER else if (poition == 1) cellvalues.horizontalAlignment = Element.ALIGN_LEFT else if (poition == 2) cellvalues.horizontalAlignment = Element.ALIGN_RIGHT
        return cellvalues
    }

    @Throws(IOException::class, BadElementException::class)
    fun setImage(): Image {
        val ims = assets.open("rupees.png")
        val bmp = BitmapFactory.decodeStream(ims)
        val stream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val img = Image.getInstance(stream.toByteArray())
        img.scaleAbsolute(12f, 12f)
        return img
    }

    private fun viewPdf(fileName: String, directory: String) {
        val file = File(Environment.getExternalStorageDirectory().toString() + File.separator + directory + File.separator + fileName)
        Log.d("viewPdf", "in viewPdf")
        val target = Intent(Intent.ACTION_VIEW)
        val photoURI = FileProvider.getUriForFile(this, this.applicationContext.packageName + ".provider", file)
        target.setDataAndType(photoURI, "application/pdf")
        target.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        val intent = Intent.createChooser(target, "Open File")
        startActivity(intent)
    }

    val isValid: Boolean
        get() {
            if (TextUtils.isEmpty(regValue!!.text.toString().trim { it <= ' ' })) {
                regValue!!.error = "Please Provide Area"
                return false
            } else if (regValue!!.text.toString().contains("-")) {
                regValue!!.error = "Please Enter Proper number"
                return false
            } else if (regValue!!.text.toString() == "0" || regValue!!.text.toString() == "00" || regValue!!.text.toString() == "000" || regValue!!.text.toString() == "0000" || regValue!!.text.toString() == "00000") {
                regValue!!.error = "Please Enter Proper number"
                return false
            } else if (regValue!!.text.toString() == "." || regValue!!.text.toString() == ".." || regValue!!.text.toString() == "..." || regValue!!.text.toString() == "...." || regValue!!.text.toString() == ".....") {
                regValue!!.error = "Please Enter Proper number"
                return false
            }
            return true
        }
    var adapter_marketLocation: Adapter_MarketLocation? = null
    var adapter_agefactor: Adapter_Agefactor? = null
    var adapter_buildingfactor: Adapter_Buildingfactor? = null
    var adapter_occupancyfactor: Adapter_Occupancyfactor? = null
    fun setAdapter() {
        adapter_marketLocation = Adapter_MarketLocation(this, Locationlist!!)
        spinnerMarketLocation!!.adapter = adapter_marketLocation
        adapter_agefactor = Adapter_Agefactor(this, Agefactorlist!!)
        spinnerAgeFactor!!.adapter = adapter_agefactor
        adapter_buildingfactor = Adapter_Buildingfactor(this, Buildingfactorlist!!)
        spinnerBuildingFactor!!.adapter = adapter_buildingfactor
        adapter_occupancyfactor = Adapter_Occupancyfactor(this, occupancylist!!)
        spinnerOccupancyfactor!!.adapter = adapter_occupancyfactor
        spinnerMarketLocation!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                locationWeitage = spinnerMarketLocation!!.selectedItem as Double
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        spinnerAgeFactor!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                agefactorWeitage = spinnerAgeFactor!!.selectedItem as Double
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        spinnerBuildingFactor!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                buildingfactorWeitage = spinnerBuildingFactor!!.selectedItem as Double
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        spinnerOccupancyfactor!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
                occupancyWeitage = spinnerOccupancyfactor!!.selectedItem as Double
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        spinnerMarketLocation!!.setOnTouchListener { v: View?, event: MotionEvent? ->
            val imm = applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(regValue!!.windowToken, 0)
            false
        }
        spinnerAgeFactor!!.setOnTouchListener { v: View?, event: MotionEvent? ->
            val imm = applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(regValue!!.windowToken, 0)
            false
        }
        spinnerBuildingFactor!!.setOnTouchListener { v: View?, event: MotionEvent? ->
            val imm = applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(regValue!!.windowToken, 0)
            false
        }
        spinnerOccupancyfactor!!.setOnTouchListener { v: View?, event: MotionEvent? ->
            val imm = applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(regValue!!.windowToken, 0)
            false
        }
        taxBtnCalculate!!.setOnTouchListener { v: View?, event: MotionEvent? ->
            val imm = applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(regValue!!.windowToken, 0)
            false
        }
        llcalTaxGone!!.setOnTouchListener { v: View?, event: MotionEvent? ->
            val imm = applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(regValue!!.windowToken, 0)
            false
        }
        llMainLayout!!.setOnTouchListener { v: View?, event: MotionEvent? ->
            val imm = applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(regValue!!.windowToken, 0)
            false
        }
    }

    @OnClick(R.id.btn_residential, R.id.btn_nonresidential)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btn_residential -> {
                isResidential = true
                dataAndSetInSpinner
                setAdapter()
                setSelectionType()
            }
            R.id.btn_nonresidential -> {
                isResidential = false
                dataAndSetInSpinner
                setAdapter()
                setSelectionType()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.menuFaqs -> FAQs()
            R.id.menuHistory -> startActivity(Intent(context, HistoryTaxCalculation::class.java).putExtra(Constants.CITY_ID, cityID).putExtra(Constants.PROPERTY_ID, propertyID))
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun FAQs() {
        if (cityID == 1) {
            val browser = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.rmc.gov.in/rmcwebsite/docs/Carpet_Rules_English.pdf"))
            startActivity(browser)
        } else if (cityID == 2) {
            val browser = Intent(Intent.ACTION_VIEW, Uri.parse("https://ahmedabadcity.gov.in/portal/jsp/Static_pages/assessment__tax_static.jsp"))
            startActivity(browser)
        } else if (cityID == 3) {
            val browser = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.suratmunicipal.gov.in/Departments/PropertyTaxStructure"))
            startActivity(browser)
        }
    }

    companion object {
        private const val STORAGE_CODE = 1000
    }
}