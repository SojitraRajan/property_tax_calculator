package com.aswdc.propertytaxcalculator.activity

import android.Manifest.permission
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.aswdc.propertytaxcalculator.DB_Helper.TblHistory
import com.aswdc.propertytaxcalculator.R
import com.aswdc.propertytaxcalculator.Utility.HeaderFooterPageEvent
import com.aswdc.propertytaxcalculator.model.Modelhistory
import com.itextpdf.text.*
import com.itextpdf.text.html.HtmlTags
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class DetailBill : AppCompatActivity() {
    var tvdetcarpet: TextView? = null
    var tvdetlocation: TextView? = null
    var tvdetage: TextView? = null
    var tvdetbuilding: TextView? = null
    var tvdetoccuoancy: TextView? = null
    var tvdettotaltax: TextView? = null
    var tvdeteducation: TextView? = null
    var tvdetgarbage: TextView? = null
    var tvdetstreetlight: TextView? = null
    var tvdetwater: TextView? = null
    var tvdetwaterdraindge: TextView? = null
    var tvdetconservancy: TextView? = null
    var tvtotalproperty: TextView? = null
    var hiscarpet: TextView? = null
    var hislocation: TextView? = null
    var hisage: TextView? = null
    var hisbuilding: TextView? = null
    var hisoccupancy: TextView? = null
    var histotaltax: TextView? = null
    var hisgarbage: TextView? = null
    var hiseducation: TextView? = null
    var hisstreetlight: TextView? = null
    var hiswater: TextView? = null
    var hisconservancy: TextView? = null
    var hiswaterdraindge: TextView? = null
    var histotalpropertytax: TextView? = null
    var hispropertydetail: TextView? = null
    var hisenvironment: TextView? = null
    var hisfire: TextView? = null
    var tvdetfire: TextView? = null
    var tvdetenvironment: TextView? = null
    var modelhistory: Modelhistory? = null
    var llgarbage: LinearLayout? = null
    var lleducation: LinearLayout? = null
    var llwater: LinearLayout? = null
    var llwaterdraindge: LinearLayout? = null
    var llstreet: LinearLayout? = null
    var llconservancy: LinearLayout? = null
    var llfire: LinearLayout? = null
    var llenvironment: LinearLayout? = null
    var tblHistory: TblHistory? = null
    var cityId = 0
    var historyId = 0
    var getPropertyId = 0
    var taxbtnshare: AppCompatButton? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_bill)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        cityId = intent.getIntExtra("CityId", -1)
        getPropertyId = intent.getIntExtra("PropertyId", -1)
        historyId = intent.getIntExtra("HistoryId", -1)
        intialization()
    }

    fun intialization() {
        hispropertydetail = findViewById<View>(R.id.propertyDetail1) as TextView
        tvdetcarpet = findViewById<View>(R.id.cal_txt_carpet_area1) as TextView
        tvdetlocation = findViewById<View>(R.id.cal_txt_location_factor1) as TextView
        tvdetage = findViewById<View>(R.id.cal_txt_Age_factor1) as TextView
        tvdetbuilding = findViewById<View>(R.id.cal_txt_building_factor1) as TextView
        tvdetoccuoancy = findViewById<View>(R.id.cal_txt_occupancy_factor1) as TextView
        tvdettotaltax = findViewById<View>(R.id.cal_txt_normal_charge1) as TextView
        tvdeteducation = findViewById<View>(R.id.cal_txt_education_charge1) as TextView
        tvdetgarbage = findViewById<View>(R.id.cal_txt_garbage_charge1) as TextView
        tvdetfire = findViewById<View>(R.id.cal_txt_fire1) as TextView
        tvdetenvironment = findViewById<View>(R.id.cal_txt_environment1) as TextView
        tvdetwater = findViewById<View>(R.id.cal_txt_watercharge1) as TextView
        tvdetconservancy = findViewById<View>(R.id.cal_txt_conservancycharge1) as TextView
        tvdetstreetlight = findViewById<View>(R.id.cal_txt_streeightlight1) as TextView
        tvdetwaterdraindge = findViewById<View>(R.id.cal_txt_water_drainage1) as TextView
        tvtotalproperty = findViewById<View>(R.id.cal_txt_total_property_tax1) as TextView
        hiscarpet = findViewById<View>(R.id.carpet_area1) as TextView
        hislocation = findViewById<View>(R.id.location_factor1) as TextView
        hisage = findViewById<View>(R.id.age_factor1) as TextView
        hisbuilding = findViewById<View>(R.id.building_factor1) as TextView
        hisoccupancy = findViewById<View>(R.id.occupancy_factor1) as TextView
        histotaltax = findViewById<View>(R.id.totaltax1) as TextView
        hiseducation = findViewById<View>(R.id.educationcharge1) as TextView
        hisgarbage = findViewById<View>(R.id.garbagecharge1) as TextView
        hisstreetlight = findViewById<View>(R.id.streetlight1) as TextView
        hiswater = findViewById<View>(R.id.watercharge1) as TextView
        hisconservancy = findViewById<View>(R.id.conservancycharge1) as TextView
        hisstreetlight = findViewById<View>(R.id.streetlight1) as TextView
        hiswaterdraindge = findViewById<View>(R.id.watgraindge1) as TextView
        hisenvironment = findViewById<View>(R.id.envinronment1) as TextView
        hisfire = findViewById<View>(R.id.fire1) as TextView
        histotalpropertytax = findViewById<View>(R.id.totalpropertytax1) as TextView
        lleducation = findViewById<View>(R.id.ll_education1) as LinearLayout
        llfire = findViewById<View>(R.id.ll_fire1) as LinearLayout
        llenvironment = findViewById<View>(R.id.ll_environment1) as LinearLayout
        llgarbage = findViewById<View>(R.id.ll_garbage1) as LinearLayout
        llwater = findViewById<View>(R.id.ll_water1) as LinearLayout
        llconservancy = findViewById<View>(R.id.ll_Conservancy1) as LinearLayout
        llwaterdraindge = findViewById<View>(R.id.ll_water_drainage1) as LinearLayout
        llstreet = findViewById<View>(R.id.ll_street1) as LinearLayout
        taxbtnshare = findViewById<View>(R.id.shareitnow1) as AppCompatButton
        tblHistory = TblHistory(this@DetailBill)
        modelhistory = tblHistory!!.selectByHistoryID(cityId, historyId)
        tvdetcarpet!!.text = modelhistory.getCarpetarea()
        tvdetlocation!!.text = modelhistory.getLocationfactor()
        tvdetage!!.text = modelhistory.getAgefactor()
        tvdetbuilding!!.text = modelhistory.getTypeofbuiling()
        tvdetoccuoancy!!.text = modelhistory.getOccupancyfactor()
        tvdettotaltax!!.text = modelhistory.getTotaltax()
        tvdetgarbage!!.text = modelhistory.getGarbagecharge()
        tvdeteducation!!.text = modelhistory.getEducationcharge()
        tvdetwater!!.text = modelhistory.getWatercharge()
        tvdetconservancy!!.text = modelhistory.getConservancycharge()
        tvdetwaterdraindge!!.text = modelhistory.getWateranddrainagecharge()
        tvdetenvironment!!.text = modelhistory.getEnvironmentcharge()
        tvdetfire!!.text = modelhistory.getFirecharge()
        tvdetstreetlight!!.text = modelhistory.getStreetlightcharge()
        tvtotalproperty!!.text = modelhistory.getTotalpropertytax()
        if (cityId == 1 && getPropertyId == 1) {
            llgarbage!!.visibility = View.VISIBLE
            lleducation!!.visibility = View.VISIBLE
            llwater!!.visibility = View.VISIBLE
        } else if (cityId == 1 && getPropertyId == 2) {
            llgarbage!!.visibility = View.VISIBLE
            lleducation!!.visibility = View.VISIBLE
            llwater!!.visibility = View.GONE
        } else if (cityId == 2) {
            lleducation!!.visibility = View.VISIBLE
            llgarbage!!.visibility = View.VISIBLE
            llwater!!.visibility = View.VISIBLE
            llconservancy!!.visibility = View.VISIBLE
        } else if (cityId == 3) {
            lleducation!!.visibility = View.VISIBLE
            llwaterdraindge!!.visibility = View.VISIBLE
            llgarbage!!.visibility = View.VISIBLE
            llstreet!!.visibility = View.VISIBLE
            llfire!!.visibility = View.VISIBLE
            llenvironment!!.visibility = View.VISIBLE
        }
        taxbtnshare!!.setOnClickListener {
            try {
                if (checkPermission()) createPDF() else {
                    requestPermission()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: DocumentException) {
                e.printStackTrace()
            }
        }
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this.applicationContext, permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(permission.WRITE_EXTERNAL_STORAGE), 200)
    }

    @Throws(IOException::class, DocumentException::class)
    fun createPDF() {
        val doc = Document(PageSize.A4, 36, 36, 90, 36)
        val directoryName = "Property Tax Calculator" + File.separator + "Property Tax calculator"
        val fileName = "PropertyTax_History.pdf"
        val path = Environment.getExternalStorageDirectory().absoluteFile.toString() + File.separator + directoryName
        val root = File(path)
        if (!root.exists()) root.mkdirs()
        val writer = PdfWriter.getInstance(doc,
                FileOutputStream(Environment.getExternalStorageDirectory().toString() + File.separator + directoryName + File.separator + fileName))
        Log.d("FilePath", this.application.getExternalFilesDir("Return").toString() + "/" + fileName)
        if (cityId == 1) {
            val strHeader = "Rajkot"
            val event = HeaderFooterPageEvent(this, strHeader)
            writer.pageEvent = event
        } else if (cityId == 2) {
            val strHeader = "Ahemdabad"
            val event = HeaderFooterPageEvent(this, strHeader)
            writer.pageEvent = event
        }
        if (cityId == 3) {
            val strHeader = "Surat"
            val event = HeaderFooterPageEvent(this, strHeader)
            writer.pageEvent = event
        }
        doc.open()
        doc.newPage()
        val reportBody = Paragraph()
        reportBody.alignment = Paragraph.ALIGN_LEFT
        val columnwidths = floatArrayOf(1.5f, 1f)
        val f1 = FontFactory.getFont(HtmlTags.FONT, 30f)
        f1.style = Font.BOLD
        val f2 = FontFactory.getFont(HtmlTags.FONT, 20f)
        f2.style = Font.BOLD
        val f8 = FontFactory.getFont(HtmlTags.FONT, 16f)
        f8.style = Font.BOLD
        val table = PdfPTable(columnwidths)
        table.widthPercentage = 100f
        table.defaultCell.isUseAscender = true
        val cell21 = PdfPCell(Phrase("Property Detail", f2))
        cell21.horizontalAlignment = Element.ALIGN_BASELINE or Element.ALIGN_BOTTOM
        cell21.border = Rectangle.BOTTOM
        cell21.paddingTop = 40f
        cell21.paddingBottom = 20f
        cell21.paddingRight = 10f
        val cell22 = PdfPCell(Phrase("  ", f2))
        cell22.horizontalAlignment = Element.ALIGN_BASELINE or Element.ALIGN_BOTTOM
        cell22.border = Rectangle.BOTTOM
        cell22.paddingTop = 40f
        cell22.paddingBottom = 20f
        cell22.paddingLeft = 10f
        cell22.paddingRight = 10f
        table.addCell(cell21)
        table.addCell(cell22)
        val mText = tvdetcarpet!!.text.toString()
        val mText1 = tvdetlocation!!.text.toString()
        val mText2 = tvdetage!!.text.toString()
        val mText3 = tvdetbuilding!!.text.toString()
        val mText4 = tvdetoccuoancy!!.text.toString()
        val mText5 = tvdettotaltax!!.text.toString()
        val mText6 = tvtotalproperty!!.text.toString()
        val rgarbage = tvdetgarbage!!.text.toString()
        val reducation = tvdeteducation!!.text.toString()
        val rwater = tvdetwater!!.text.toString()
        val rngarbage = tvdetgarbage!!.text.toString()
        val rneducation = tvdeteducation!!.text.toString()
        val agarbage = tvdetgarbage!!.text.toString()
        val aeducation = tvdeteducation!!.text.toString()
        val awater = tvdetwater!!.text.toString()
        val aconversancy = tvdetconservancy!!.text.toString()
        val sgarbage = tvdetgarbage!!.text.toString()
        val seducation = tvdeteducation!!.text.toString()
        val senvrionment = tvdetenvironment!!.text.toString()
        val sfire = tvdetfire!!.text.toString()
        val swaterdraindge = tvdetwaterdraindge!!.text.toString()
        val sstreetlight = tvdetstreetlight!!.text.toString()
        if (cityId == 1 && getPropertyId == 1) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Residential" + " "))
        }
        if (cityId == 1 && getPropertyId == 2) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Non-Residential" + " "))
        }
        if (cityId == 2 && getPropertyId == 1) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Residential" + " "))
        }
        if (cityId == 2 && getPropertyId == 2) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Non-Residential" + " "))
        }
        if (cityId == 3 && getPropertyId == 1) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Residential" + " "))
        }
        if (cityId == 3 && getPropertyId == 2) {
            table.addCell(setCellTitle("Property Type"))
            table.addCell(setCellTitle1(" " + "Non-Residential" + " "))
        }
        table.addCell(setCellTitle("Carpet Area"))
        table.addCell(setCellTitle1(" $mText "))
        table.addCell(setCellTitle("Location Factor"))
        table.addCell(setCellTitle1("  $mText1 "))
        table.addCell(setCellTitle("Age Factor"))
        table.addCell(setCellTitle1("  $mText2 "))
        table.addCell(setCellTitle("Building  Factor"))
        table.addCell(setCellTitle1("  $mText3 "))
        table.addCell(setCellTitle("Occupancy  Factor"))
        table.addCell(setCellTitle1("  $mText4 "))
        table.addCell(setCellTitle("Total  Tax"))
        table.addCell(setCellvalue1("$mText5 "))
        if (cityId == 1 && getPropertyId == 1) {
            table.addCell(setCellTitle("Garbage Charge"))
            table.addCell(setCellvalue1("$rgarbage "))
            table.addCell(setCellTitle("Education Charge"))
            table.addCell(setCellvalue1("$reducation "))
            table.addCell(setCellTitle("Water Charge"))
            table.addCell(setCellvalue1("$rwater "))
        } else if (cityId == 1 && getPropertyId == 2) {
            table.addCell(setCellTitle("Garbage Charge"))
            table.addCell(setCellvalue1("$rngarbage "))
            table.addCell(setCellTitle("Education Charge"))
            table.addCell(setCellvalue1("$rneducation "))
        } else if (cityId == 2) {
            table.addCell(setCellTitle("Garbage Charge"))
            table.addCell(setCellvalue1("$agarbage "))
            table.addCell(setCellTitle("Education Charge"))
            table.addCell(setCellvalue1("$aeducation "))
            table.addCell(setCellTitle("Water Charge"))
            table.addCell(setCellvalue1("$awater "))
            table.addCell(setCellTitle("Conservancy Charge"))
            table.addCell(setCellvalue1("$aconversancy "))
        } else if (cityId == 3) {
            table.addCell(setCellTitle("Garbage Charge"))
            table.addCell(setCellvalue1("$sgarbage "))
            table.addCell(setCellTitle("Education Charge"))
            table.addCell(setCellvalue1("$seducation "))
            table.addCell(setCellTitle("Waterdraindge Charge"))
            table.addCell(setCellvalue1("$swaterdraindge "))
            table.addCell(setCellTitle("Fire Charge"))
            table.addCell(setCellvalue1("$sfire "))
            table.addCell(setCellTitle("Environment Charge"))
            table.addCell(setCellvalue1("$senvrionment "))
            table.addCell(setCellTitle("Streetlight Charge"))
            table.addCell(setCellvalue1("$sstreetlight "))
        }
        table.addCell(setHeaderCellvalue("Total Property Tax", f8, 1))
        table.addCell(setHeaderCellvalue1("$mText6 ", f8, 2))
        reportBody.add(table)
        doc.add(reportBody)
        doc.close()
        viewPdf(fileName, directoryName)
    }

    fun setCellTitle(str: String?): PdfPCell {
        val f3 = FontFactory.getFont(HtmlTags.FONT, 16f)
        f3.setColor(0, 0, 0)
        val cell41 = PdfPCell(Phrase(str, f3))
        cell41.border = Rectangle.LEFT or Rectangle.BOTTOM or Rectangle.RIGHT
        cell41.horizontalAlignment = Element.ALIGN_BASELINE or Element.ALIGN_BOTTOM
        cell41.setPadding(8f)
        return cell41
    }

    fun setCellTitle1(str: String): PdfPCell {
        val f3 = FontFactory.getFont(HtmlTags.FONT, 16f)
        f3.setColor(0, 0, 0)
        val cell41 = PdfPCell(Phrase(str, f3))
        val p = Paragraph()
        cell41.border = Rectangle.LEFT or Rectangle.BOTTOM or Rectangle.RIGHT
        cell41.horizontalAlignment = Element.ALIGN_BASELINE or Element.ALIGN_BOTTOM
        cell41.setPadding(8f)
        p.add(Phrase(" $str", f3))
        p.alignment = Element.ALIGN_RIGHT
        cell41.addElement(p)
        return cell41
    }

    @Throws(IOException::class, DocumentException::class)
    fun setCellvalue1(str: String): PdfPCell {
        val f4 = FontFactory.getFont(HtmlTags.FONT, 16f)
        f4.setColor(0, 0, 0)
        val cell42 = PdfPCell()
        cell42.border = Rectangle.BOTTOM or Rectangle.RIGHT //new Phrase(str, f4));
        val p = Paragraph()
        cell42.horizontalAlignment = Element.ALIGN_BASELINE or Element.ALIGN_RIGHT
        p.add(Chunk(setImage(), 0, 0, true))
        p.add(Phrase(" $str", f4))
        p.alignment = Element.ALIGN_RIGHT
        cell42.addElement(p)
        cell42.setPadding(8f)
        return cell42
    }

    fun setHeaderCellvalue(str: String?, f: Font?, poition: Int): PdfPCell {
        val cellvalues = PdfPCell(Phrase(str, f))
        cellvalues.horizontalAlignment = Element.ALIGN_CENTER
        cellvalues.border = Rectangle.BOX
        cellvalues.setPadding(8f)
        cellvalues.backgroundColor = BaseColor.WHITE
        if (poition == 0) cellvalues.horizontalAlignment = Element.ALIGN_CENTER else if (poition == 1) cellvalues.horizontalAlignment = Element.ALIGN_LEFT else if (poition == 2) cellvalues.horizontalAlignment = Element.ALIGN_RIGHT
        return cellvalues
    }

    fun setHeaderCellvalue1(str: String, f: Font?, poition: Int): PdfPCell {
        val cellvalues = PdfPCell(Phrase(str, f))
        cellvalues.horizontalAlignment = Element.ALIGN_CENTER
        cellvalues.border = Rectangle.BOX
        cellvalues.setPadding(5f)
        val p = Paragraph()
        try {
            p.add(Chunk(setImage(), 0, 0, true))
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: BadElementException) {
            e.printStackTrace()
        }
        p.alignment = Element.ALIGN_RIGHT
        p.add(Phrase(" $str", f))
        cellvalues.addElement(p)
        cellvalues.backgroundColor = BaseColor.WHITE
        if (poition == 0) cellvalues.horizontalAlignment = Element.ALIGN_CENTER else if (poition == 1) cellvalues.horizontalAlignment = Element.ALIGN_LEFT else if (poition == 2) cellvalues.horizontalAlignment = Element.ALIGN_RIGHT
        return cellvalues
    }

    @Throws(IOException::class, BadElementException::class)
    fun setImage(): Image {
        val ims = assets.open("rupees.png")
        val bmp = BitmapFactory.decodeStream(ims)
        val stream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val img = Image.getInstance(stream.toByteArray())
        img.scaleAbsolute(12f, 12f)
        return img
    }

    private fun viewPdf(fileName: String, directory: String) {
        val file = File(Environment.getExternalStorageDirectory().toString() + File.separator + directory + File.separator + fileName)
        Log.d("viewPdf", "in viewPdf")
        val target = Intent(Intent.ACTION_VIEW)
        val photoURI = FileProvider.getUriForFile(this, this.applicationContext.packageName + ".provider", file)
        target.setDataAndType(photoURI, "application/pdf")
        target.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        val intent = Intent.createChooser(target, "Open File")
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return true
    }
}