package com.aswdc.propertytaxcalculator.activity

import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import com.aswdc.propertytaxcalculator.BuildConfig
import com.aswdc.propertytaxcalculator.R
import com.aswdc.propertytaxcalculator.Utility.Constants
import java.util.*

class Design_DeveloperActivity : AppCompatActivity() {
    var toolbar: Toolbar? = null
    var icmail: TextView? = null
    var icphone: TextView? = null
    var icweb: TextView? = null
    var icshare: TextView? = null
    var icapp: TextView? = null
    var icrate: TextView? = null
    var iclike: TextView? = null
    var icupdate: TextView? = null
    var appinfo: TextView? = null
    var tvPrivacy: TextView? = null
    var Devevloper: WebView? = null
    var tvyear: TextView? = null
    var email: LinearLayout? = null
    var call: LinearLayout? = null
    var web: LinearLayout? = null
    var share: LinearLayout? = null
    var moreapps: LinearLayout? = null
    var rate: LinearLayout? = null
    var likefb: LinearLayout? = null
    var update: LinearLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_design__developer)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        title = "Developer"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        icmail = findViewById<View>(R.id.dev_ic_mail) as TextView
        icphone = findViewById<View>(R.id.dev_ic_phone) as TextView
        icweb = findViewById<View>(R.id.dev_ic_web) as TextView
        icshare = findViewById<View>(R.id.dev_ic_share) as TextView
        icapp = findViewById<View>(R.id.dev_ic_app) as TextView
        icrate = findViewById<View>(R.id.dev_ic_rate) as TextView
        iclike = findViewById<View>(R.id.dev_ic_like) as TextView
        icupdate = findViewById<View>(R.id.dev_ic_update) as TextView
        appinfo = findViewById<View>(R.id.dev_tv_appinfo) as TextView
        Devevloper = findViewById<View>(R.id.dev_tv_aswdcinfo) as WebView
        tvyear = findViewById<View>(R.id.developer_tv_year) as TextView
        tvPrivacy = findViewById(R.id.dev_tv_privacy)
        email = findViewById<View>(R.id.dev_email) as LinearLayout
        call = findViewById<View>(R.id.dev_call) as LinearLayout
        web = findViewById<View>(R.id.dev_web) as LinearLayout
        share = findViewById<View>(R.id.dev_share) as LinearLayout
        moreapps = findViewById<View>(R.id.dev_more_apps) as LinearLayout
        rate = findViewById<View>(R.id.dev_rate) as LinearLayout
        likefb = findViewById<View>(R.id.dev_like_fb) as LinearLayout
        update = findViewById<View>(R.id.dev_update) as LinearLayout
        val materialface = Typeface.createFromAsset(assets, "MaterialFont.ttf")
        icmail!!.setTypeface(materialface)
        icphone!!.setTypeface(materialface)
        icweb!!.setTypeface(materialface)
        icshare!!.setTypeface(materialface)
        icapp!!.setTypeface(materialface)
        icrate!!.setTypeface(materialface)
        iclike!!.setTypeface(materialface)
        icupdate!!.setTypeface(materialface)
        val year = Calendar.getInstance()[Calendar.YEAR]
        tvyear!!.text = Html.fromHtml("© $year Darshan Institute of Engineering &amp; Technology")
        Devevloper!!.loadDataWithBaseURL("", "<html><body align=\"justify\" style=\"font-size:15px;color:#747474\">ASWDC is Application, Software and Website Development Center @ Darshan Engineering College run by Students and Staff of Computer Engineering Department.<br><br> Sole purpose of ASWDC is to bridge gap between university curriculum &amp; industry demands. Students learn cutting edge technologies, develop real world application &amp; experiences professional environment @ ASWDC under guidance of industry experts &amp; faculty members", "text/html", "utf-8", "")
        Devevloper!!.setOnLongClickListener { true }
        Devevloper!!.isLongClickable = false
        appinfo!!.text = Html.fromHtml(resources.getString(R.string.app_name) + " (v" + BuildConfig.VERSION_NAME + ")")
        email!!.setOnClickListener {
            val emailintent = Intent("android.intent.action.SENDTO", Uri.fromParts("mailto", Constants.ASWDCEmailAddress, null))
            emailintent.putExtra("android.intent.extra.SUBJECT", "Contact from " + getString(R.string.app_name))
            startActivity(Intent.createChooser(emailintent, "Send Email to ASWDC"))
        }
        call!!.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:+" + Constants.AdminMobileNo)
            startActivity(intent)
        }
        web!!.setOnClickListener {
            val webintent = Intent("android.intent.action.VIEW", Uri.parse("http://www.darshan.ac.in"))
            startActivity(webintent)
        }
        share!!.setOnClickListener {
            val share = Intent()
            share.action = "android.intent.action.SEND"
            share.type = "text/plain"
            share.putExtra("android.intent.extra.TEXT", Constants.SHARE_MESSAGE)
            startActivity(share)
        }
        rate!!.setOnClickListener {
            val rateintent = Intent("android.intent.action.VIEW", Uri.parse("market://details?id=$packageName"))
            startActivity(rateintent)
        }
        moreapps!!.setOnClickListener {
            val moreappsintent = Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:Darshan+Institute+of+Engineering+%26+Technology"))
            startActivity(moreappsintent)
        }
        likefb!!.setOnClickListener {
            val fbintent = Intent("android.intent.action.VIEW", Uri.parse("https://www.facebook.com/DarshanInstitute.Official"))
            startActivity(fbintent)
        }
        update!!.setOnClickListener {
            val updateintent = Intent("android.intent.action.VIEW", Uri.parse("market://details?id=$packageName"))
            startActivity(updateintent)
        }
        tvPrivacy!!.setOnClickListener(View.OnClickListener {
            val webintent = Intent("android.intent.action.VIEW", Uri.parse("http://www.darshan.ac.in/DIET/ASWDC-Mobile-Apps/Privacy-Policy-General"))
            startActivity(webintent)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }
}