package com.aswdc.propertytaxcalculator.activity

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.aswdc.propertytaxcalculator.DB_Helper.TblHistory
import com.aswdc.propertytaxcalculator.R
import com.aswdc.propertytaxcalculator.Utility.Constants
import com.aswdc.propertytaxcalculator.adapter.Adapter_History
import com.aswdc.propertytaxcalculator.model.Modelhistory
import java.util.*

class HistoryTaxCalculation : AppCompatActivity() {
    var cityId = 0
    var getPropertyID = 0
    protected var context: Context? = null
    protected var billListView: ListView? = null
    var modelhistory: ArrayList<Modelhistory>? = null
    protected var tblHistory: TblHistory? = null
    var adapter_history: Adapter_History? = null
    var llerror: LinearLayout? = null
    var tvhis: TextView? = null
    var btnNonresidential: TextView? = null
    var btnResidential: TextView? = null
    var flag = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.activity_history_tax_calculation)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        context = this@HistoryTaxCalculation
        tblHistory = TblHistory(this)
        initView()
        dataFromIntent
        cityTitilte()
        if (getPropertyID == 1) {
            btnResidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.left_selected))
            btnResidential!!.setTextColor(ContextCompat.getColor(context, R.color.white))
            btnNonresidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.right_unselected))
            btnNonresidential!!.setTextColor(ContextCompat.getColor(context, R.color.gray))
            adapter_history = Adapter_History(this, TblHistory(this).getBillList(cityId, 1), 1, cityId)
            if (adapter_history!!.isEmpty) {
                tvhis!!.text = "NO DATA FOUND!"
                llerror!!.visibility = View.VISIBLE
            } else {
                billListView!!.adapter = adapter_history
            }
            flag = 0
        } else if (getPropertyID == 2) {
            btnNonresidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.right_selected))
            btnNonresidential!!.setTextColor(ContextCompat.getColor(context, R.color.white))
            btnResidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.left_unselected))
            btnResidential!!.setTextColor(ContextCompat.getColor(context, R.color.gray))
            adapter_history = Adapter_History(this, TblHistory(this).getBillList(cityId, 2), 2, cityId)
            if (adapter_history!!.isEmpty) {
                tvhis!!.text = "NO DATA FOUND!"
                llerror!!.visibility = View.VISIBLE
            } else {
                billListView!!.adapter = adapter_history
            }
            flag = 1
        }
        btnNonresidential!!.setOnClickListener {
            llerror!!.visibility = View.GONE
            btnNonresidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.right_selected))
            btnNonresidential!!.setTextColor(ContextCompat.getColor(context, R.color.white))
            btnResidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.left_unselected))
            btnResidential!!.setTextColor(ContextCompat.getColor(context, R.color.gray))
            flag = 1
            adapter_history = Adapter_History(this@HistoryTaxCalculation, TblHistory(this@HistoryTaxCalculation).getBillList(cityId, 2), 2, cityId)
            if (adapter_history!!.isEmpty) {
                tvhis!!.text = "NO DATA FOUND!"
                llerror!!.visibility = View.VISIBLE
            } else {
                billListView!!.adapter = adapter_history
            }
        }
        btnResidential!!.setOnClickListener {
            llerror!!.visibility = View.GONE
            btnResidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.left_selected))
            btnResidential!!.setTextColor(ContextCompat.getColor(context, R.color.white))
            btnNonresidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.right_unselected))
            btnNonresidential!!.setTextColor(ContextCompat.getColor(context, R.color.gray))
            flag = 0
            adapter_history = Adapter_History(this@HistoryTaxCalculation, TblHistory(this@HistoryTaxCalculation).getBillList(cityId, 1), 1, cityId)
            if (adapter_history!!.isEmpty) {
                tvhis!!.text = "NO DATA FOUND!"
                llerror!!.visibility = View.VISIBLE
            } else {
                billListView!!.adapter = adapter_history
            }
        }
    }

    private fun initView() {
        billListView = findViewById<View>(R.id.bill_listView) as ListView
        llerror = findViewById<View>(R.id.llhiserror) as LinearLayout
        tvhis = findViewById<View>(R.id.txthis) as TextView
        btnResidential = findViewById<View>(R.id.btn_residential1) as TextView
        btnNonresidential = findViewById<View>(R.id.btn_nonresidential1) as TextView
        btnResidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.left_selected))
        btnResidential!!.setTextColor(ContextCompat.getColor(this, R.color.white))
        btnNonresidential!!.setBackgroundDrawable(resources.getDrawable(R.drawable.right_unselected))
        btnNonresidential!!.setTextColor(ContextCompat.getColor(context!!, R.color.gray))
    }

    val dataFromIntent: Unit
        get() {
            cityId = intent.getIntExtra(Constants.CITY_ID, -1)
            getPropertyID = intent.getIntExtra(Constants.PROPERTY_ID, -1)
        }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_delete_all, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.bill_delete_all -> if (adapter_history!!.isEmpty) {
                val builder = AlertDialog.Builder(this@HistoryTaxCalculation)
                builder.setMessage("Please enter data")
                        .setPositiveButton("Ok", null)
                val alert = builder.create()
                alert.show()
            } else {
                val builder = AlertDialog.Builder(this@HistoryTaxCalculation)
                builder.setMessage("Are you sure you want delete all history?")
                        .setPositiveButton("Delete") { dialogInterface, i ->
                            tblHistory!!.deleteAllBill(modelhistory)
                            adapter_history = if (flag == 1) Adapter_History(this@HistoryTaxCalculation, TblHistory(this@HistoryTaxCalculation).getBillList(cityId, 2), 2, cityId) else Adapter_History(this@HistoryTaxCalculation, TblHistory(this@HistoryTaxCalculation).getBillList(cityId, 1), 1, cityId)
                            billListView!!.adapter = adapter_history
                            tvhis!!.text = "NO DATA FOUND!"
                            llerror!!.visibility = View.VISIBLE
                        }.setTitle("Alert")
                        .setNegativeButton("Cancel", null)
                val alert = builder.create()
                alert.show()
            }
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return true
    }

    fun cityTitilte() {
        if (cityId == 1) {
            title = "History Tax (Rajkot)"
        } else if (cityId == 2) {
            title = "History Tax (Ahemdabad)"
        } else if (cityId == 3) {
            title = "History Tax (Surat)"
        }
    }
}