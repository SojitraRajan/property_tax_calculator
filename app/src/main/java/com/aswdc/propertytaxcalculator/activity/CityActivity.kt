package com.aswdc.propertytaxcalculator.activity

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.aswdc.propertytaxcalculator.model.Modelcity
import com.aswdc.propertytaxcalculator.adapter.Adapter_City
import android.os.Bundle
import com.aswdc.propertytaxcalculator.R
import androidx.recyclerview.widget.GridLayoutManager
import com.aswdc.propertytaxcalculator.DB_Helper.TblCity
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import com.aswdc.propertytaxcalculator.activity.Design_DeveloperActivity
import android.widget.Toast
import com.aswdc.propertytaxcalculator.Utility.Constants
import java.util.ArrayList

class CityActivity : AppCompatActivity() {
    var recyclerView: RecyclerView? = null
    var arrayCity: ArrayList<Modelcity>? = null
    var adapter_city: Adapter_City? = null
    var doubleBackToExitPressedOnce = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.city_inputedittext)
        title = "Select City"
        initReference()
        setAdapter()
    }

    fun initReference() {
        recyclerView = findViewById(R.id.recyclerview)
        arrayCity = TblCity().selectCities()
    }

    fun setAdapter() {
        adapter_city = Adapter_City(this, arrayCity)
        recyclerView!!.adapter = adapter_city
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.dashboard_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.iteam_checkupdate -> {
            }
            R.id.iteam_feedback -> moreApp()
            R.id.item_other -> moreApp()
            R.id.item_share -> shareApp()
            R.id.iteam_developer -> startActivity(Intent(this, Design_DeveloperActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    private fun shareApp() {
        val share = Intent()
        share.action = "android.intent.action.SEND"
        share.type = "text/plain"
        share.putExtra("android.intent.extra.TEXT", Constants.SHARE_MESSAGE)
        startActivity(share)
    }

    private fun moreApp() {
        val moreAppsIntent = Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:Darshan+Institute+of+Engineering+%26+Technology"))
        startActivity(moreAppsIntent)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
}