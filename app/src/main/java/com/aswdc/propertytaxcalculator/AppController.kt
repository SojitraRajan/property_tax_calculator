package com.aswdc.propertytaxcalculator

import android.app.Application

class AppController : Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        var instance: AppController? = null
    }
}