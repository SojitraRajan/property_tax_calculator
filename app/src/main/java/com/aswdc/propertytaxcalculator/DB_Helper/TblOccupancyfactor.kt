package com.aswdc.propertytaxcalculator.DB_Helper

import android.content.Context
import com.aswdc.propertytaxcalculator.Utility.Constants
import com.aswdc.propertytaxcalculator.model.Modeloccupancyfactor
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper
import java.util.*

class TblOccupancyfactor(context: Context?) : SQLiteAssetHelper(context, Constants.dbName, null, null, Constants.dbVersion) {
    fun selectOccupancyfactor(CityID: Int, PropertyTypeID: Int): ArrayList<Modeloccupancyfactor> {
        val db = readableDatabase
        val strQuery = "select * from PTC_OccupancyfactorF4 where CityId = $CityID and PropertyTypeId = $PropertyTypeID"
        val cur = db.rawQuery(strQuery, null)
        val arrayReg = ArrayList<Modeloccupancyfactor>()
        if (cur.moveToFirst()) {
            do {
                val bof = Modeloccupancyfactor()
                bof.occupancyfactorId = cur.getInt(cur.getColumnIndex("OccupancyfactorId"))
                bof.occupancyTypeName = cur.getString(cur.getColumnIndex("OccupancyTypeName"))
                bof.weitage = cur.getDouble(cur.getColumnIndex("Weitage"))
                arrayReg.add(bof)
            } while (cur.moveToNext())
        }
        db.close()
        return arrayReg
    }
}