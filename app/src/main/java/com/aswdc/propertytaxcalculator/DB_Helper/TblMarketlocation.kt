package com.aswdc.propertytaxcalculator.DB_Helper

import android.content.Context
import com.aswdc.propertytaxcalculator.Utility.Constants
import com.aswdc.propertytaxcalculator.model.ModelmarketLocation
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper
import java.util.*

class TblMarketlocation(context: Context?) : SQLiteAssetHelper(context, Constants.dbName, null, null, Constants.dbVersion) {
    fun selectlocation(City: Int): ArrayList<ModelmarketLocation> {
        val db = readableDatabase
        val strQuery = "select * from PTC_LocationfactorF1 where CityId=$City"
        val cur = db.rawQuery(strQuery, null)
        val arrayReg = ArrayList<ModelmarketLocation>()
        if (cur.moveToFirst()) {
            do {
                val bml = ModelmarketLocation()
                bml.locationfactorId = cur.getInt(cur.getColumnIndex("LocationfactorId"))
                bml.locationfactorName = cur.getString(cur.getColumnIndex("LocationfactorName"))
                bml.weitage = cur.getDouble(cur.getColumnIndex("Weitage"))
                arrayReg.add(bml)
            } while (cur.moveToNext())
        }
        db.close()
        return arrayReg
    }
}