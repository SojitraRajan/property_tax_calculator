package com.aswdc.propertytaxcalculator.DB_Helper

import com.aswdc.propertytaxcalculator.AppController
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper

open class DBHelper : SQLiteAssetHelper(AppController.Companion.getInstance(), dbName, null, dbVersion) {
    companion object {
        var dbName = "PropertyTax.db"
        var dbVersion = 1
    }
}