package com.aswdc.propertytaxcalculator.DB_Helper

import android.content.Context
import com.aswdc.propertytaxcalculator.Utility.Constants
import com.aswdc.propertytaxcalculator.model.Modelagefactor
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper
import java.util.*

class TblAgefactor(context: Context?) : SQLiteAssetHelper(context, Constants.dbName, null, null, Constants.dbVersion) {
    fun selectlAgefactor(CityID: Int): ArrayList<Modelagefactor> {
        val db = readableDatabase
        val strQuery = "select * from PTC_AgefactorF2 where CityId=$CityID"
        val cur = db.rawQuery(strQuery, null)
        val arrayReg = ArrayList<Modelagefactor>()
        if (cur.moveToFirst()) {
            do {
                val baf = Modelagefactor()
                baf.agefactorId = cur.getInt(cur.getColumnIndex("AgefactorId"))
                baf.agefactorName = cur.getString(cur.getColumnIndex("AgefactorName"))
                baf.weitage = cur.getDouble(cur.getColumnIndex("Weitage"))
                arrayReg.add(baf)
            } while (cur.moveToNext())
        }
        db.close()
        return arrayReg
    }
}