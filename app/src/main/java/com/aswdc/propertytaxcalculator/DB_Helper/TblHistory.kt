package com.aswdc.propertytaxcalculator.DB_Helper

import android.content.ContentValues
import android.content.Context
import android.database.SQLException
import android.util.Log
import com.aswdc.propertytaxcalculator.Utility.Constants
import com.aswdc.propertytaxcalculator.model.Modelhistory
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper
import java.util.*

class TblHistory(context: Context?) : SQLiteAssetHelper(context, Constants.dbName, null, null, Constants.dbVersion) {
    fun insert(modelhistory: Modelhistory) {
        val db = writableDatabase
        val cv = ContentValues()
        cv.put("CityId", modelhistory.cityId)
        cv.put("PropertyTypeId", modelhistory.propertyTypeId)
        cv.put("Carpetarea", modelhistory.carpetarea)
        cv.put("Locationfactor", modelhistory.locationfactor)
        cv.put("Agefactor", modelhistory.agefactor)
        cv.put("Typeofbuiling", modelhistory.typeofbuiling)
        cv.put("Occupancyfactor", modelhistory.occupancyfactor)
        cv.put("Totaltax", modelhistory.totaltax)
        cv.put("Educationcharge", modelhistory.educationcharge)
        cv.put("Garbagecharge", modelhistory.garbagecharge)
        cv.put("Watercharge", modelhistory.watercharge)
        cv.put("Firecharge", modelhistory.firecharge)
        cv.put("Environmentcharge", modelhistory.environmentcharge)
        cv.put("Conservancycharge", modelhistory.conservancycharge)
        cv.put("Wateranddrainagecharge", modelhistory.wateranddrainagecharge)
        cv.put("Streetlightcharge", modelhistory.wateranddrainagecharge)
        cv.put("Totalpropertytax", modelhistory.totalpropertytax)
        db.insert("PTC_History", null, cv)
        db.close()
    }

    fun getBillList(cityId: Int, propertyId: Int): ArrayList<Modelhistory> {
        val db = readableDatabase
        val cursor = db.rawQuery("SELECT * FROM PTC_History INNER JOIN PTC_City ON PTC_History.CityId = PTC_City.CityId   where PTC_History.CityId=$cityId and PTC_History.PropertyTypeId=$propertyId ORDER By PTC_History.HistoryId DESC ", null)
        val list = ArrayList<Modelhistory>()
        var modelhistory: Modelhistory
        if (cursor.moveToFirst()) {
            do {
                modelhistory = Modelhistory()
                modelhistory.cityId = cursor.getInt(cursor.getColumnIndex("CityId"))
                modelhistory.cityName = cursor.getString(cursor.getColumnIndex("CityName"))
                modelhistory.historyId = cursor.getInt(cursor.getColumnIndex("HistoryId"))
                modelhistory.carpetarea = cursor.getString(cursor.getColumnIndex("Carpetarea"))
                modelhistory.totalpropertytax = cursor.getString(cursor.getColumnIndex("Totalpropertytax"))
                Log.d("123465", "" + propertyId)
                list.add(modelhistory)
            } while (cursor.moveToNext())
        }
        db.close()
        cursor.close()
        return list
    }

    fun getAllList(cityId: Int, propertyId: Int): ArrayList<Modelhistory> {
        val db = readableDatabase
        val cursor = db.rawQuery("SELECT * FROM PTC_History INNER JOIN PTC_City ON PTC_History.CityId = PTC_City.CityId   where PTC_History.CityId=$cityId and PTC_History.PropertyTypeId=$propertyId ORDER By PTC_History.HistoryId DESC ", null)
        val list = ArrayList<Modelhistory>()
        var modelhistory: Modelhistory
        if (cursor.moveToFirst()) {
            do {
                modelhistory = Modelhistory()
                modelhistory.historyId = cursor.getInt(cursor.getColumnIndex("HistoryId"))
                modelhistory.carpetarea = cursor.getString(cursor.getColumnIndex("Carpetarea"))
                modelhistory.occupancyfactor = cursor.getString(cursor.getColumnIndex("Occupancyfactor"))
                modelhistory.totalpropertytax = cursor.getString(cursor.getColumnIndex("Totalpropertytax"))
                modelhistory.cityId = cursor.getInt(cursor.getColumnIndex("CityId"))
                modelhistory.locationfactor = cursor.getString(cursor.getColumnIndex("Locationfactor"))
                modelhistory.agefactor = cursor.getString(cursor.getColumnIndex("Agefactor"))
                modelhistory.typeofbuiling = cursor.getString(cursor.getColumnIndex("Typeofbuiling"))
                modelhistory.totaltax = cursor.getString(cursor.getColumnIndex("Totaltax"))
                modelhistory.garbagecharge = cursor.getString(cursor.getColumnIndex("Garbagecharge"))
                modelhistory.educationcharge = cursor.getString(cursor.getColumnIndex("Educationcharge"))
                modelhistory.streetlightcharge = cursor.getString(cursor.getColumnIndex("Streetlightcharge"))
                modelhistory.watercharge = cursor.getString(cursor.getColumnIndex("Watercharge"))
                modelhistory.firecharge = cursor.getString(cursor.getColumnIndex("Firecharge"))
                modelhistory.environmentcharge = cursor.getString(cursor.getColumnIndex("Environmentcharge"))
                modelhistory.wateranddrainagecharge = cursor.getString(cursor.getColumnIndex("Wateranddrainagecharge"))
                modelhistory.conservancycharge = cursor.getString(cursor.getColumnIndex("Conservancycharge"))
                list.add(modelhistory)
            } while (cursor.moveToNext())
        }
        db.close()
        cursor.close()
        return list
    }

    fun selectByHistoryID(cityId: Int, historyId: Int): Modelhistory {
        val db = readableDatabase
        val strQuery = "SELECT * FROM PTC_History INNER JOIN PTC_City ON PTC_History.CityId = PTC_City.CityId   where PTC_History.CityId=$cityId and PTC_History.HistoryId=$historyId"
        val cursor = db.rawQuery(strQuery, null)
        val modelhistory = Modelhistory()
        if (cursor.moveToFirst()) {
            modelhistory.historyId = cursor.getInt(cursor.getColumnIndex("HistoryId"))
            modelhistory.carpetarea = cursor.getString(cursor.getColumnIndex("Carpetarea"))
            modelhistory.occupancyfactor = cursor.getString(cursor.getColumnIndex("Occupancyfactor"))
            modelhistory.totalpropertytax = cursor.getString(cursor.getColumnIndex("Totalpropertytax"))
            modelhistory.cityId = cursor.getInt(cursor.getColumnIndex("CityId"))
            modelhistory.locationfactor = cursor.getString(cursor.getColumnIndex("Locationfactor"))
            modelhistory.agefactor = cursor.getString(cursor.getColumnIndex("Agefactor"))
            modelhistory.typeofbuiling = cursor.getString(cursor.getColumnIndex("Typeofbuiling"))
            modelhistory.totaltax = cursor.getString(cursor.getColumnIndex("Totaltax"))
            modelhistory.garbagecharge = cursor.getString(cursor.getColumnIndex("Garbagecharge"))
            modelhistory.educationcharge = cursor.getString(cursor.getColumnIndex("Educationcharge"))
            modelhistory.streetlightcharge = cursor.getString(cursor.getColumnIndex("Streetlightcharge"))
            modelhistory.watercharge = cursor.getString(cursor.getColumnIndex("Watercharge"))
            modelhistory.firecharge = cursor.getString(cursor.getColumnIndex("Firecharge"))
            modelhistory.environmentcharge = cursor.getString(cursor.getColumnIndex("Environmentcharge"))
            modelhistory.wateranddrainagecharge = cursor.getString(cursor.getColumnIndex("Wateranddrainagecharge"))
            modelhistory.conservancycharge = cursor.getString(cursor.getColumnIndex("Conservancycharge"))
        }
        db.close()
        return modelhistory
    }

    fun deleteAllBill(modelhistory: ArrayList<Modelhistory?>?): Boolean {
        val database = writableDatabase
        return try {
            database.execSQL("DELETE FROM PTC_History ")
            database.close()
            true
        } catch (e: SQLException) {
            database.close()
            false
        }
    }
}