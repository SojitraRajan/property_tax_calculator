package com.aswdc.propertytaxcalculator.DB_Helper

import android.util.Log
import com.aswdc.propertytaxcalculator.model.Modelcity
import java.util.*

class TblCity : DBHelper() {
    fun selectCities(): ArrayList<Modelcity> {
        val db = readableDatabase
        val strQuery = "select CityId,CityName from PTC_City"
        val cur = db.rawQuery(strQuery, null)
        val arrayReg = ArrayList<Modelcity>()
        if (cur.moveToFirst()) {
            do {
                val bc = Modelcity()
                bc.cityId = cur.getInt(cur.getColumnIndex("CityId"))
                Log.d("123", "" + cur.getInt(cur.getColumnIndex("CityId")))
                bc.cityName = cur.getString(cur.getColumnIndex("CityName"))
                arrayReg.add(bc)
            } while (cur.moveToNext())
        }
        db.close()
        return arrayReg
    }
}