package com.aswdc.propertytaxcalculator.DB_Helper

import android.content.Context
import com.aswdc.propertytaxcalculator.Utility.Constants
import com.aswdc.propertytaxcalculator.model.Modelbuilingfactor
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper
import java.util.*

class TBLBuildingFactor(context: Context?) : SQLiteAssetHelper(context, Constants.dbName, null, null, Constants.dbVersion) {
    fun selectBuildingfactor(CityID: Int, PropertyTypeId: Int): ArrayList<Modelbuilingfactor> {
        val db = readableDatabase
        val strQuery = "select " +
                "* " +
                "from " +
                "PTC_PropertyfactorF3 " +
                "where " +
                "CityId = " + CityID +
                " and PropertyTypeId = " + PropertyTypeId
        val cur = db.rawQuery(strQuery, null)
        val arrayReg = ArrayList<Modelbuilingfactor>()
        if (cur.moveToFirst()) {
            do {
                val bbf = Modelbuilingfactor()
                bbf.proprtyfactorId = cur.getInt(cur.getColumnIndex("ProprtyfactorId"))
                bbf.propertyfactorName = cur.getString(cur.getColumnIndex("PropertyfactorName"))
                bbf.weitage = cur.getDouble(cur.getColumnIndex("Weitage"))
                arrayReg.add(bbf)
            } while (cur.moveToNext())
        }
        db.close()
        return arrayReg
    }
}