package com.aswdc.propertytaxcalculator.Utility

import android.content.Context
import android.content.SharedPreferences

class PrefHelper {
    private val PREF_NAME = "aswdc_propertytaxcalculator"
    private var preferences: SharedPreferences? = null
    private fun getPreferences(context: Context): SharedPreferences? {
        if (preferences == null) {
            preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        }
        return preferences
    }

    fun saveCity(context: Context, lang: Int) {
        getPreferences(context)!!.edit().putInt(Constants.CITY_ID, lang).apply()
    }

    fun getCity(context: Context): Int {
        return getPreferences(context)!!.getInt(Constants.CITY_ID, -1)
    }

    companion object {
        private var prefHelper: PrefHelper? = null
        val instance: PrefHelper?
            get() {
                if (prefHelper == null) {
                    prefHelper = PrefHelper()
                }
                return prefHelper
            }
    }
}