package com.aswdc.propertytaxcalculator.Utility

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.itextpdf.text.*
import com.itextpdf.text.pdf.*
import java.io.ByteArrayOutputStream
import java.io.IOException

class HeaderFooterPageEvent(private val activity: Activity, str: String) : PdfPageEventHelper() {
    var strTitle = ""
    private var t: PdfTemplate? = null
    private var total: Image? = null
    override fun onOpenDocument(writer: PdfWriter, document: Document) {
        t = writer.directContent.createTemplate(30f, 16f)
        try {
            total = Image.getInstance(t)
            total.setRole(PdfName.ARTIFACT)
        } catch (de: DocumentException) {
            throw ExceptionConverter(de)
        }
    }

    override fun onEndPage(writer: PdfWriter, document: Document) {
        addHeader(writer)
        addFooter(writer)
    }

    private fun addHeader(writer: PdfWriter) {
        val header = PdfPTable(1)
        try {
            // set defaults
            header.setWidths(intArrayOf(1))
            header.totalWidth = 527f
            header.isLockedWidth = true
            header.defaultCell.fixedHeight = 40f
            header.defaultCell.border = Rectangle.BOTTOM
            header.defaultCell.borderColor = BaseColor.BLACK
            val text = PdfPCell()
            text.paddingBottom = 15f
            text.border = Rectangle.BOTTOM
            text.borderColor = BaseColor.BLUE
            text.addElement(Phrase(strTitle, Font(Font.FontFamily.HELVETICA, 30, Font.BOLD)))
            text.horizontalAlignment = Element.ALIGN_CENTER
            header.horizontalAlignment = Element.ALIGN_MIDDLE
            header.addCell(text)


            // write content
//header.writeSelectedRows()
            header.writeSelectedRows(0, -1, 34f, 803f, writer.directContent)
        } catch (de: DocumentException) {
            throw ExceptionConverter(de)
        }
    }

    private fun addFooter(writer: PdfWriter) {
        val footer = PdfPTable(2)
        try {
            // set defaults
            footer.setWidths(intArrayOf(4, 22))
            footer.totalWidth = 527f
            footer.isLockedWidth = true
            footer.defaultCell.border = Rectangle.TOP
            footer.defaultCell.borderColor = BaseColor.BLUE

            // add copyright
            val logo = setImage()
            footer.addCell(logo)

            // add text
            val text = PdfPCell()
            text.setPadding(10f)
            text.border = Rectangle.TOP
            text.borderColor = BaseColor.BLUE
            text.addElement(Phrase("Property Tax Calculator", Font(Font.FontFamily.HELVETICA, 12, Font.BOLD)))
            footer.horizontalAlignment = Element.ALIGN_LEFT
            footer.addCell(text)
            val canvas = writer.directContent
            canvas.beginMarkedContentSequence(PdfName.ARTIFACT)
            footer.writeSelectedRows(0, -1, 34f, 80f, canvas)
            canvas.endMarkedContentSequence()
        } catch (de: DocumentException) {
            throw ExceptionConverter(de)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onCloseDocument(writer: PdfWriter, document: Document) {
        val totalLength = writer.pageNumber.toString().length
        val totalWidth = totalLength * 5
        ColumnText.showTextAligned(t, Element.ALIGN_RIGHT,
                Phrase(writer.pageNumber.toString(), Font(Font.FontFamily.HELVETICA, 8)),
                totalWidth.toFloat(), 6f, 0f)
    }

    @Throws(IOException::class, BadElementException::class)
    fun setImage(): Image {
        val ims = activity.assets.open("cornerproperty1.jpg")
        val bmp = BitmapFactory.decodeStream(ims)
        val stream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val img = Image.getInstance(stream.toByteArray())
        img.scaleAbsolute(12f, 8f)
        return img
    }

    init {
        strTitle = str
    }
}