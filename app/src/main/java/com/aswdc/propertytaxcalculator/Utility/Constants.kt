package com.aswdc.propertytaxcalculator.Utility

object Constants {
    var dbName = "PropertyTax.db"
    var dbVersion = 1
    const val CITY_ID = "CityId"
    const val PROPERTY_ID = "PropertyId"
    var AdminMobileNo = "+91-9428231065"
    var ASWDCEmailAddress = "aswdc@darshan.ac.in"
    var SHARE_MESSAGE = "I just downloaded an App \"Property Tax Calculator\". You can also download it from Play Store: \nhttp://tiny.cc/Property tax Calculator"
}