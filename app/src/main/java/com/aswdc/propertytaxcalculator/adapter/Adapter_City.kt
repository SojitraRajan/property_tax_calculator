package com.aswdc.propertytaxcalculator.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aswdc.propertytaxcalculator.R
import com.aswdc.propertytaxcalculator.Utility.Constants
import com.aswdc.propertytaxcalculator.activity.CalculateTaxActivity
import com.aswdc.propertytaxcalculator.adapter.Adapter_City.Adapter_CityViewHolder
import com.aswdc.propertytaxcalculator.model.Modelcity
import com.squareup.picasso.Picasso
import java.util.*

class Adapter_City(var mContext: Context, var arrayCity: ArrayList<Modelcity>) : RecyclerView.Adapter<Adapter_CityViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter_CityViewHolder {
        return Adapter_CityViewHolder(LayoutInflater.from(mContext).inflate(R.layout.card_view_iteam_city, null))
    }

    override fun onBindViewHolder(holder: Adapter_CityViewHolder, position: Int) {
        holder.tv_cityname.text = arrayCity[position].cityName
        if (arrayCity[position].cityName.equals("Rajkot", ignoreCase = true)) {
            Picasso.get().load(R.mipmap.ic_rcity).into(holder.img_city)
        } else if (arrayCity[position].cityName.equals("Ahmedabad", ignoreCase = true)) {
            Picasso.get().load(R.mipmap.ic_acity).into(holder.img_city)
        } else if (arrayCity[position].cityName.equals("Surat", ignoreCase = true)) {
            Picasso.get().load(R.mipmap.ic_scity).into(holder.img_city)
        }
        holder.llmain.setOnClickListener {
            val `in` = Intent(mContext, CalculateTaxActivity::class.java)
            `in`.putExtra(Constants.CITY_ID, arrayCity[position].cityId)
            mContext.startActivity(`in`)
        }
    }

    override fun getItemCount(): Int {
        return arrayCity.size
    }

    inner class Adapter_CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_cityname: TextView
        var img_city: ImageView
        var llmain: LinearLayout

        init {
            tv_cityname = itemView.findViewById(R.id.city_name_id)
            img_city = itemView.findViewById(R.id.city_img_id)
            llmain = itemView.findViewById(R.id.cardview_id)
        }
    }
}