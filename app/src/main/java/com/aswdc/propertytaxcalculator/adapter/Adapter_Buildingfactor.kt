package com.aswdc.propertytaxcalculator.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.aswdc.propertytaxcalculator.R
import com.aswdc.propertytaxcalculator.model.Modelbuilingfactor
import java.util.*

class Adapter_Buildingfactor(var mContext: Context, var buildingfactor: ArrayList<Modelbuilingfactor>) : BaseAdapter() {
    override fun getCount(): Int {
        return buildingfactor.size
    }

    override fun getItem(i: Int): Any {
        return buildingfactor[i].weitage
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    private inner class ViewHolder {
        var tvbuildingfactor: TextView? = null
    }

    override fun getView(i: Int, view: View, viewGroup: ViewGroup): View {
        val viewHolder: ViewHolder
        var v = view
        if (v == null) {
            v = LayoutInflater.from(mContext).inflate(R.layout.sp_buildingfactor, null)
            viewHolder = ViewHolder()
            viewHolder.tvbuildingfactor = v.findViewById(R.id.sp_tv_building_factor)
            v.tag = viewHolder
        } else {
            viewHolder = v.tag as ViewHolder
        }
        viewHolder.tvbuildingfactor.setText(buildingfactor[i].propertyfactorName)
        return v
    }
}