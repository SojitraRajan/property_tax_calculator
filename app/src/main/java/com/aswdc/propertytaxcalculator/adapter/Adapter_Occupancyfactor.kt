package com.aswdc.propertytaxcalculator.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.aswdc.propertytaxcalculator.R
import com.aswdc.propertytaxcalculator.model.Modeloccupancyfactor
import java.util.*

class Adapter_Occupancyfactor(var mContext: Context, var occupancyfactor: ArrayList<Modeloccupancyfactor>) : BaseAdapter() {
    override fun getCount(): Int {
        return occupancyfactor.size
    }

    override fun getItem(i: Int): Any {
        return occupancyfactor[i].weitage
    }

    override fun getItemId(i: Int): Long {
        return occupancyfactor[i].occupancyfactorId.toLong()
    }

    inner class ViewHolder {
        var tvoccupancyname: TextView? = null
    }

    override fun getView(i: Int, view: View, viewGroup: ViewGroup): View {
        val viewHolder: ViewHolder
        var v = view
        if (v == null) {
            v = LayoutInflater.from(mContext).inflate(R.layout.sp_occupancyfactor, null)
            viewHolder = ViewHolder()
            viewHolder.tvoccupancyname = v.findViewById(R.id.tv_occupancy_factor)
            v.tag = viewHolder
        } else {
            viewHolder = v.tag as ViewHolder
        }
        viewHolder.tvoccupancyname?.setText(occupancyfactor[i].occupancyTypeName)
        return v
    }
}