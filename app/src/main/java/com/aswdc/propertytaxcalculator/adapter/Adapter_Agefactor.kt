package com.aswdc.propertytaxcalculator.adapter

import android.content.Context
import com.aswdc.propertytaxcalculator.model.Modelagefactor
import android.widget.BaseAdapter
import android.widget.TextView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.aswdc.propertytaxcalculator.R
import java.util.ArrayList

class Adapter_Agefactor(var mContext: Context, var agefactorList: ArrayList<Modelagefactor>) : BaseAdapter() {
    override fun getCount(): Int {
        return agefactorList.size
    }

    override fun getItem(i: Int): Any {
        return agefactorList[i].weitage
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    private inner class ViewHolder {
        var tvagefactorname: TextView? = null
    }

    override fun getView(i: Int, view: View, viewGroup: ViewGroup): View {
        val viewHolder: ViewHolder
        var v = view
        if (v == null) {
            v = LayoutInflater.from(mContext).inflate(R.layout.sp_agefactor, null)
            viewHolder = ViewHolder()
            viewHolder.tvagefactorname = v.findViewById(R.id.sp_tv_age_factor)
            v.tag = viewHolder
        } else {
            viewHolder = v.tag as ViewHolder
        }
        viewHolder.tvagefactorname!!.text = agefactorList[i].agefactorName
        return v
    }
}