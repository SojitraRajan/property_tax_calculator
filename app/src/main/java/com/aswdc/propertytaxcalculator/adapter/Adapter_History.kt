package com.aswdc.propertytaxcalculator.adapter

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import com.aswdc.propertytaxcalculator.DB_Helper.TblHistory
import com.aswdc.propertytaxcalculator.R
import com.aswdc.propertytaxcalculator.activity.DetailBill
import com.aswdc.propertytaxcalculator.model.Modelhistory
import java.util.*

class Adapter_History(var act: Activity, var arrayAdd: ArrayList<Modelhistory>, var propertyType: Int, var cityId: Int) : BaseAdapter() {
    var tblHistory: TblHistory
    override fun getCount(): Int {
        return arrayAdd.size
    }

    override fun getItem(position: Int): Any {
        return arrayAdd[position]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    private inner class ViewHolder {
        var listBillPropertyTypeId: TextView? = null
        var listBillTvCarpetArea: TextView? = null
        var listBillTvTotalPropertyTax: TextView? = null
        var listBillHistpryId: TextView? = null
        var listBillLlReviewBill: LinearLayout? = null
        var typeface: Typeface? = null
    }

    override fun getView(i: Int, view: View, viewGroup: ViewGroup): View {
        var view = view
        val viewHolder: ViewHolder
        val inflater = act.layoutInflater
        if (view == null) {
            view = inflater.inflate(R.layout.card_view_iteam_bill, null)
            viewHolder = ViewHolder()
            viewHolder.listBillHistpryId = view.findViewById<View>(R.id.list_bill_historyid) as TextView
            viewHolder.listBillLlReviewBill = view.findViewById<View>(R.id.list_bill_ll_clickToReviewBill) as LinearLayout
            viewHolder.listBillPropertyTypeId = view.findViewById<View>(R.id.list_bill_propertytype) as TextView
            viewHolder.listBillTvCarpetArea = view.findViewById<View>(R.id.list_bill_carpet_area) as TextView
            viewHolder.listBillTvTotalPropertyTax = view.findViewById<View>(R.id.list_bill_total_property_tax) as TextView
            view.tag = viewHolder
        } else {
            viewHolder = view.tag as ViewHolder
        }
        if (propertyType == 1) {
            viewHolder.listBillPropertyTypeId!!.text = "Residential"
        } else if (propertyType == 2) {
            viewHolder.listBillPropertyTypeId!!.text = "Non-Residential"
        }
        viewHolder.listBillHistpryId.setText(arrayAdd[i].historyId.toString())
        viewHolder.listBillTvCarpetArea.setText(arrayAdd[i].carpetarea)
        viewHolder.listBillTvTotalPropertyTax.setText(arrayAdd[i].totalpropertytax)
        viewHolder.listBillLlReviewBill!!.setOnClickListener {
            val `in` = Intent(act, DetailBill::class.java)
            `in`.putExtra("CityId", arrayAdd[i].cityId)
            `in`.putExtra("PropertyId", propertyType)
            `in`.putExtra("HistoryId", arrayAdd[i].historyId)
            act.startActivity(`in`)
        }
        return view
    }

    init {
        tblHistory = TblHistory(act)
    }
}