package com.aswdc.propertytaxcalculator.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.aswdc.propertytaxcalculator.R
import com.aswdc.propertytaxcalculator.model.ModelmarketLocation
import java.util.*

class Adapter_MarketLocation(var mContext: Context, var locationList: ArrayList<ModelmarketLocation>) : BaseAdapter() {
    override fun getCount(): Int {
        return locationList.size
    }

    override fun getItem(i: Int): Any {
        return locationList[i].weitage
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    inner class ViewHolder {
        var tvlocationname: TextView? = null
    }

    override fun getView(i: Int, view: View, viewGroup: ViewGroup): View {
        val viewHolder: ViewHolder
        var v = view
        if (v == null) {
            v = LayoutInflater.from(mContext).inflate(R.layout.sp_marketlocation, null)
            viewHolder = ViewHolder()
            viewHolder.tvlocationname = v.findViewById(R.id.sp_market_tv_locationname)
            v.tag = viewHolder
        } else {
            viewHolder = v.tag as ViewHolder
        }
        viewHolder.tvlocationname.setText(locationList[i].locationfactorName)
        return v
    }
}