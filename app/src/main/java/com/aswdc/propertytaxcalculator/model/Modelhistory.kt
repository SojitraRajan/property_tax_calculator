package com.aswdc.propertytaxcalculator.model

class Modelhistory {
    var historyId = 0
    var cityId = 0
    var environmentcharge: String? = null
    var firecharge: String? = null
    var propertyTypeId = 0
    var carpetarea: String? = null
    var locationfactor: String? = null
    var agefactor: String? = null
    var cityName: String? = null
    var occupancyTypeName: String? = null
    var propertyTypeName: String? = null
    var typeofbuiling: String? = null
    var educationcharge: String? = null
    var garbagecharge: String? = null
    var watercharge: String? = null
    var conservancycharge: String? = null
    var wateranddrainagecharge: String? = null
    var streetlightcharge: String? = null
    var totalpropertytax: String? = null
    var occupancyfactor: String? = null
    var totaltax: String? = null
}