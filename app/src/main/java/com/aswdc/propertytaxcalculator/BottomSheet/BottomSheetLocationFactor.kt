package com.aswdc.propertytaxcalculator.BottomSheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aswdc.propertytaxcalculator.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomSheetLocationFactor : BottomSheetDialogFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.tooltip, container, false)
    }
}